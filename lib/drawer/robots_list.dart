import 'package:flutter/material.dart';
import 'package:robocomp_app/generated/i18n.dart';


class Robot {
  final String robotName;
  final String robotCategory;
  final int robotId;

  Robot(
      {@required this.robotName,
        @required this.robotCategory,
        @required this.robotId});

}

class RobotList extends StatefulWidget {
  @override
  _RobotListState createState() => _RobotListState();
}

class _RobotListState extends State<RobotList> {
  void _showRobotInfo(Robot robot) {
    Navigator.of(context)
        .popAndPushNamed("/robot_details", arguments: robot.robotId);
  }

  Widget _buildRobotItem(Robot robot) {
    final fullRobotId = RichText(
      text: TextSpan(children: [
        TextSpan(text: robot.robotName),
        TextSpan(
            text: "#",
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white54)),
        TextSpan(
            text: robot.robotId.toString(),
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white54))
      ]),
    );

    final robotTile = Material(
        borderOnForeground: true,
        color: Color(0xff373a46),
        child: InkWell(
          onTap: () {
            _showRobotInfo(robot);
          },
          child: ListTile(
            title: fullRobotId,
          ),
        ));
    return robotTile;
  }

  Widget createLayout(BuildContext context) {
    final List<Robot> _robots = [
      Robot(robotName: "Jaszczurek", robotCategory: "MicroMouse", robotId: 10),
      Robot(
          robotName: "Jaszczurekv2",
          robotCategory: "Linefollower",
          robotId: 10122),
      Robot(
          robotName: "Jaszczurek-beta", robotCategory: "Sumo", robotId: 1015522),
      Robot(
          robotName: "Jaszczurek-alfa", robotCategory: "Sumo", robotId: 10152522),
    ];

    final robotList = ExpansionTile(
            backgroundColor: Color(0xff373a46),
            title: Text(
              S.of(context).my_robots,
              style: TextStyle(color: Colors.grey),
            ),
            onExpansionChanged: (bool) {},
            children: _robots.map(_buildRobotItem).toList()
    );

    return Flexible(
      fit: FlexFit.tight,
      child: robotList,
    );
  }

  @override
  Widget build(BuildContext context) {
    return createLayout(context);
  }
}
