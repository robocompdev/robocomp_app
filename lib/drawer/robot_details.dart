import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:robocomp_app/generated/i18n.dart';


class RobotDetails extends StatelessWidget {
  void showZoomedQr(BuildContext context, int robotId) {
    GlobalKey qrPopupKey = GlobalKey();

    final qrCode = Container(
      key: qrPopupKey,
      child: QrImage(
        data: robotId.toString(),
        version: 4,
        errorCorrectionLevel: 3,
        //high level, cannot find appropriate enum value
        backgroundColor: Colors.white,
        padding: EdgeInsets.all(15.0),
        gapless: true,
      ),
    );

    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
              elevation: 10.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                padding: EdgeInsets.all(5.0),
                child: qrCode,
              ));
        });
  }

  Widget _createRobotDetails(
      BuildContext context, int robotId) {

    final robotNameText = Container(
      child: RichText(
        text: TextSpan(
            text: "Jaszczurek",//todo:change data provider
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white54, fontSize: 20.0)),
      ),
    );

    final robotNameLabel = Container(
      child: RichText(
        text: TextSpan(text: S.of(context).robot_name, style: TextStyle(fontSize: 20.0)),
      ),
    );

    final robotIdText = Container(
      child: RichText(
        text: TextSpan(
            text: "#$robotId",
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white54, fontSize: 20.0)),
      ),
    );

    final robotIdLabel = Container(
      child: RichText(
        text: TextSpan(text: S.of(context).robot_id, style: TextStyle(fontSize: 20.0)),
      ),
    );

    final teamNameText = Container(
      child: RichText(
        text: TextSpan(
            text: "team.teamName",//todo: to fix
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.white54, fontSize: 20.0)),
      ),
    );

    final teamNameLabel = Container(
      child: RichText(
        text: TextSpan(text: S.of(context).team_name, style: TextStyle(fontSize: 20.0)),
      ),
    );

    final details = Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              robotNameLabel,
              SizedBox(height: 20.0),
              robotIdLabel,
              SizedBox(height: 20.0),
              teamNameLabel,
              SizedBox(height: 20.0),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              robotNameText,
              SizedBox(height: 20.0),
              robotIdText,
              SizedBox(height: 20.0),
              teamNameText,
              SizedBox(height: 20.0),
            ],
          ),
        )
      ],
    );

    final qrCode = GestureDetector(
      onTap: () {
        showZoomedQr(context, robotId);
      },
      child: QrImage(
        data: robotId.toString(),
        version: 3,
        errorCorrectionLevel: 3,
        //high level, cannot find appropriate enum value
        backgroundColor: Colors.white,
        size: 200.0,
        gapless: true,
      ),
    );

    final main = Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[details, SizedBox(height: 40.0), qrCode],
        ));

    return main;
  }

  Widget _createLayout(BuildContext context, int robotId) {
    final closeButton = Builder(
        builder: (context) => IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
          color: Colors.white70,
        ));

    final _appBar = AppBar(
      leading: closeButton,
      title: Text(S.of(context).robot),
    );

    final _body = _createRobotDetails(context, robotId);

    final view = Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: _appBar,
      body: _body,
    );

    return view;
  }

  @override
  Widget build(BuildContext context) {
    final int robotId = ModalRoute.of(context).settings.arguments;
    return _createLayout(context, robotId);
  }
}