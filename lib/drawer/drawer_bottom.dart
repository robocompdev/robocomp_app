import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:robocomp_app/drawer/about_popup.dart';
import 'package:robocomp_app/drawer/report_popup.dart';
import 'package:robocomp_app/generated/i18n.dart';
import 'package:robocomp_app/models/authentication_state.dart';
import 'package:robocomp_app/models/settings.dart';


class DrawerBottom extends StatelessWidget {
  final bool isAuthenticated;
  final bool isArbiter;
  DrawerBottom({
    @required this.isAuthenticated,
    @required this.isArbiter,
  });

  Future<void> _logOut(BuildContext context) async {
    await Provider.of<AuthenticationState>(context, listen: false).signOut();
    Navigator.of(context).pushNamedAndRemoveUntil('/login_page', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final helpButton = ListTile(
      onTap: () async {
        final RESULT_STATE state = await showDialog(
            context: context,
            builder: (BuildContext context){
              return ReportPopup();
            });
        switch (state) {
          case RESULT_STATE.SENT:
            {
              Navigator.of(context).pop();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(S.of(context).message_sent),
                  duration: Duration(seconds: 3),
                  backgroundColor: Colors.blueGrey, //todo: change color
                ),
              );
              break;
            }
          case RESULT_STATE.FAILED:
            {
              Navigator.of(context).pop();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(S.of(context).something_went_wrong),
                  duration: Duration(seconds: 3),
                  backgroundColor: Colors.blueGrey, //todo: change color
                ),
              );
              break;
            }
          case RESULT_STATE.NONE:
            {}
        }
      },
      contentPadding: EdgeInsets.symmetric(horizontal: 30.0),
      leading: IconTheme(
        child: Icon(Icons.live_help),
        data: IconThemeData(color: Colors.white),
      ),
      title:
      Text(S.of(context).i_have_problem_with, style: TextStyle(color: Colors.blueAccent)),
    );

    final aboutButton = ListTile(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AboutPopup();
            });
      },
      contentPadding: EdgeInsets.symmetric(horizontal: 30.0),
      leading: IconTheme(
        child: Icon(Icons.help),
        data: IconThemeData(color: Colors.white70),
      ),
      title: Text(S.of(context).about, style: TextStyle(color: Colors.blueGrey)),
    );

    final logOutButton = ListTile(
      onTap: () async {
        await _logOut(context);
      },
      contentPadding: EdgeInsets.symmetric(horizontal: 30.0),
      leading: IconTheme(
        child: Icon(Icons.exit_to_app),
        data: IconThemeData(color: Colors.red),
      ),
      title: Text(
          isAuthenticated? S.of(context).sign_out : S.of(context).exit,
          style: TextStyle(color: Color(0xfffa3e3e))),
    );

    List<Widget> containerLayout = [];
    if(true) {//todo: check if arbiter
      final arbiterSwitch = Consumer<Settings>(
        builder: (BuildContext context, Settings settings, Widget child) {
          return SwitchListTile(
            onChanged: (bool state) {
              if(state) {
                settings.activateArbiterMode();
                Future.delayed(Duration(milliseconds: 300),
                      () {
                   Navigator.of(context)
                        .pushReplacementNamed('/arbiter_page');}
                );
              }
              else {
                settings.deactivateArbiterMode();
                Future.delayed(Duration(milliseconds: 300),
                      () {
                    Navigator.of(context).pushReplacementNamed('/category_page');
                  },);
              }
            },
            value: settings.arbiterMode,
            title: Row(
              children: <Widget>[
                Text(S.of(context).arbiter_mode, style: TextStyle(color: Colors.white)),
              ],
            ),
          );
        },
      );

      containerLayout.addAll([
        arbiterSwitch,
        Divider(
          color: Colors.white10,
        ),
      ]);
    }
    containerLayout.addAll([
      aboutButton,
      Divider(
        color: Colors.white10,
      ),
    ]);
    if(isAuthenticated) {
      containerLayout.addAll([
        helpButton,
        Divider(
          color: Colors.white10,
        ),
      ]);
    }
    containerLayout.addAll([
      logOutButton,
      SizedBox(height: 35.0),
    ]);

    return Container(
      alignment: Alignment.bottomCenter,
      child: Column(
        children: containerLayout,
      ),
    );
  }
}
