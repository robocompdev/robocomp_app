import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/generated/i18n.dart';
import 'package:robocomp_app/graphql_documents/mutations/messages.dart';

enum PROBLEM_TYPE { ROBOT_ISSUE, OTHER }
enum RESULT_STATE { NONE, SENT, FAILED }

const Map<PROBLEM_TYPE, String> problemInfoRecipient = {
  PROBLEM_TYPE.ROBOT_ISSUE: "test",
  PROBLEM_TYPE.OTHER: "kamilek",
};

class ReportPopup extends StatefulWidget {
  @override
  _ReportPopupState createState() => _ReportPopupState();
}

class _ReportPopupState extends State<ReportPopup> {
  PROBLEM_TYPE problemType = PROBLEM_TYPE.ROBOT_ISSUE;
  final _formKey = GlobalKey<FormState>();
  final textController = TextEditingController();

  void dispose() {
    textController.dispose();
    super.dispose();
  }

  Widget _createLayout(BuildContext context) {
    final Map<PROBLEM_TYPE, String> problemDescriptions = {
      PROBLEM_TYPE.ROBOT_ISSUE: S.of(context).technical_issue,
      PROBLEM_TYPE.OTHER: S.of(context).other_issue
    };
    
    final problemTypeInput = Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            alignedDropdown: true,
            child: Theme(
              data: ThemeData(canvasColor: Color(0xff6495ED)),
              child: DropdownButtonFormField<PROBLEM_TYPE>(
                value: problemType,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color(0xff6495ED),
                ),
                onChanged: (PROBLEM_TYPE newState) {
                  setState(() {
                    problemType = newState;
                  });
                },
                items: problemDescriptions.keys
                    .map<DropdownMenuItem<PROBLEM_TYPE>>((PROBLEM_TYPE type) {
                  return DropdownMenuItem<PROBLEM_TYPE>(
                    value: type,
                    child: Text(problemDescriptions[type]),
                  );
                }).toList(),
              ),
            ),
          ),
        )
      ],
    );

    final form = Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(S.of(context).problem_field_hint),
          problemTypeInput,
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              width: 300,
              height: 140,
              child: TextFormField(
                style: TextStyle(color: Colors.blueGrey),
                maxLength: 256,
                keyboardType: TextInputType.multiline,
                maxLines: 256,
                controller: textController,
                validator: (value) {
                  if (value.isEmpty) {
                    return S.of(context).report_validation_error;
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: S.of(context).report_input_hint,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Mutation(
              options: MutationOptions(
                document: REPORT_STRING,
                onCompleted: (dynamic resultData) {
                  if (resultData != null) {
                    Navigator.of(context).pop<RESULT_STATE>(RESULT_STATE.SENT);
                  }
                  else {
                    Navigator.of(context).pop<RESULT_STATE>(RESULT_STATE.FAILED);
                  }
                },
              ),
              builder: (RunMutation runMutation,
                  QueryResult result) {
                final buttonChild = result.loading ?
                CircularProgressIndicator(
                  backgroundColor: Colors.grey,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white70),
                ) :
                Text(S.of(context).submit);
                return RaisedButton(
                  color: Color(0xff820d35),
                  child: buttonChild,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      runMutation({
                        'mRecipient': problemInfoRecipient[problemType],
                        'mSubject': problemDescriptions[problemType],
                        'mText': textController.text,
                      });
                    }
                  },
                );
              },
            ),
          ),
        ],
      ),
    );

    return SimpleDialog(
      backgroundColor: Color(0xff132639),
      children: <Widget>[form],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _createLayout(context);
  }
}
