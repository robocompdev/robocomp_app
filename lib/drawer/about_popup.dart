import 'package:flutter/material.dart';


class AboutPopup extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AboutDialog(
      applicationName: "Robocomp",
      applicationVersion: "0.0.1",
    );
  }
}
