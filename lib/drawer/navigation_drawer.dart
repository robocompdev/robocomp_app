import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:robocomp_app/custom_widgets/shimmer_skeletal_text/shimmer_skeletal_text.dart';
import 'package:robocomp_app/drawer/drawer_bottom.dart';
import 'package:robocomp_app/drawer/robots_list.dart';
import 'package:robocomp_app/generated/i18n.dart';
import 'package:robocomp_app/graphql_documents/query/user.dart';
import 'package:robocomp_app/models/authentication_state.dart';

class NavigationDrawer extends StatelessWidget {
  static final NavigationDrawer _drawer = NavigationDrawer._internal();

  factory NavigationDrawer() {
    return _drawer;
  }

  Widget _buildHeader(BuildContext context, AuthenticationState state) {
    final teamLogo = Image.asset("assets/categories/sumo.png");

    final settingsButton = Material(
      color: Colors.transparent,
      elevation: 4.0,
      clipBehavior: Clip.antiAlias,
      shape: CircleBorder(),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed('/settings_page');
        },
        child: IconTheme(
          child: Icon(Icons.settings),
          data: IconThemeData(color: Colors.white70),
        ),
      ),
    );
    if(!state.isAuthenticated()) {
      return UserAccountsDrawerHeader(
          accountName: Text(S.of(context).guest),
          accountEmail: Text(S.of(context).guest),
          currentAccountPicture: CircleAvatar(
            child: teamLogo,
            backgroundColor: Colors.deepOrange,
          ),
          otherAccountsPictures: <Widget>[settingsButton],
      );
    }

    final infoQueryOptions = QueryOptions(
      document: WHO_AM_I_USER_INFO,
      pollInterval: 30,
    );

    return Query(
      options: infoQueryOptions,
      builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
        bool queryCompleted = true;
        if (result.hasException) {
          debugPrint(result.exception.toString());//todo: Error handling
          queryCompleted = false;
        }
        if (result.loading) {
          queryCompleted = false;
        }
        if(queryCompleted) {
          final nickname = result.data['whoAmI']['nickname'];
          final teamName = result.data['whoAmI']['team']['name'];

          return UserAccountsDrawerHeader(
            accountName: Text(nickname),
            accountEmail: Text(teamName),
            currentAccountPicture: CircleAvatar(
              child: teamLogo,
              backgroundColor: Colors.deepOrange,
            ),
            otherAccountsPictures: <Widget>[settingsButton],
          );
        }
        else {
          return UserAccountsDrawerHeader(
            accountName: ShimmerSkeletalText(
              duration: Duration(seconds: 1),
              width: 90.0,
              height: 15.0,
            ),
            accountEmail: ShimmerSkeletalText(
              duration: Duration(seconds: 1),
              width: 70.0,
              height: 15.0,
            ),
            currentAccountPicture: CircleAvatar(
              child: teamLogo,
              backgroundColor: Colors.deepOrange,
            ),
            otherAccountsPictures: <Widget>[settingsButton],
          );
        }
      },
    );
  }

  NavigationDrawer._internal();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color(0xff1b1e29),
      ),
      child: Drawer(
        child: Consumer<AuthenticationState>(
          builder: (context, state, child) {
            List<Widget> layout = [];
            layout.add(_buildHeader(context, state));
            if(state.privilegesLevel == PRIVILEGES_LEVEL.PARTICIPANT) {
              layout.add(RobotList());
              layout.add(Divider(color: Colors.white10));
            }
            layout.add(
                DrawerBottom(
                  isAuthenticated: state.isAuthenticated(),
                  isArbiter: state.privilegesLevel == PRIVILEGES_LEVEL.ARBITER,
                ),
            );
            return Column(
              children: layout,
            );
          },
        ),
      ),
    );
  }
}
