import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/competition_types.dart';
import 'package:robocomp_app/custom_widgets/expandable_list/expandable_element_container.dart';
import 'package:robocomp_app/custom_widgets/expandable_list/expandable_panel.dart';
import 'package:robocomp_app/graphql_documents/query/sumo.dart';

class SumoGroupPhaseSubpage extends StatelessWidget {
  final CompetitionCategory category;

  SumoGroupPhaseSubpage({
    this.category,
  });

  @override
  Widget build(BuildContext context) {
    final options = QueryOptions(
        document: SUMO_GROUP_COUNT
    );

    return Query(
      options: options,
      builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
        if (result.hasException != null) {
          return Column(
            children: <Widget>[
              Text(
                "Ups. Coś poszło nie tak",
                style: TextStyle(
                  fontSize: 30.0,
                  color: Colors.white30,
                ),
              )
            ],
          );
        }
        if (result.loading) {
          return Container(
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.scaleDown,
                  image: competitionImage[category],
                )
            ),
          );
        }
        final groupCount = result.data['sumoGroupCount'];
        return ExpandableElementContainer(
          child: ListView.builder(
            itemCount: groupCount,
            itemBuilder: (BuildContext context, int group) {
              return Text("1");
            },
          ),
        );
      },
    );
  }
}
