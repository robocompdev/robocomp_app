import 'package:flutter/material.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:robocomp_app/competition_types.dart';
import 'freestyle_phase_subpage.dart';

class FreestyleResultsPage extends StatefulWidget {
  FreestyleResultsPage({Key key}) : super(key: key);

  @override
  _FreestyleResultsPageState createState() => _FreestyleResultsPageState();
}

class _FreestyleResultsPageState extends State<FreestyleResultsPage> {
  PageController _pageViewController;
  int _page = 0;

  @override
  void initState() {
    super.initState();
    _pageViewController = PageController(initialPage: 0);
  }

  void _onPageChanged(int page) {
    setState(() {
      _page = page;
    });
  }

  void _onNavBarClicked(int index) {
    setState(() {
      _page = index;
    });
    _pageViewController.animateToPage(index,
        duration: Duration(milliseconds: 250), curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    final CompetitionCategory category = ModalRoute.of(context).settings.arguments;
    final appBarTitles = ["Kwalifikacje", "Finał"];
    Icon backIcon;

    if (Theme.of(context).platform == TargetPlatform.iOS) {
      backIcon = Icon(Icons.arrow_back_ios);
    } else {
      backIcon = Icon(Icons.arrow_back);
    }

    final backButton = Builder(
        builder: (context) => IconButton(
          icon: backIcon,
          onPressed: () {
            Navigator.of(context).pop();
          },
          color: Colors.white70,
        ));

    final pageTitle =
        competitionNames[category] + "\n" + appBarTitles[_page];

    final navBar = Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color(0xff1b1e29),
      ),
      child: BottomNavigationBar(
        showSelectedLabels: true,
        onTap: _onNavBarClicked,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white54,
        currentIndex: _page,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text(appBarTitles[0]),
            icon: Icon(
              Icons.insert_chart,
              size: 25.0,
            ),
          ),
          BottomNavigationBarItem(
            title: Text(appBarTitles[1]),
            icon: Icon(
              GroovinMaterialIcons.crown,
              size: 25.0,
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        leading: backButton,
        title: Text(pageTitle),
      ),
      bottomNavigationBar: navBar,
      backgroundColor: Color(0xff1b1e29),
      body: Theme(
        data: Theme.of(context).copyWith(
          cardColor: Colors.white10,
        ),
        child: PageView(
          scrollDirection: Axis.horizontal,
          controller: _pageViewController,
          onPageChanged: _onPageChanged,
          children: <Widget>[
            FreestylePhaseSubpage(category: category, phase: Phase.QUALIFICATION,),
            FreestylePhaseSubpage(category: category, phase: Phase.FINAL,),
          ],
        ),
      ),


    );
  }
}