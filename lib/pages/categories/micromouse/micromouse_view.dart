import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/competition_types.dart';
import 'package:robocomp_app/custom_widgets/expandable_list/expandable_element_container.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';
import 'package:robocomp_app/graphql_documents/query/micromouse.dart';


class MicromouseView extends StatefulWidget {
  @override
  _MicromouseViewState createState() => _MicromouseViewState();
}

class _MicromouseViewState extends State<MicromouseView> {
  final _controller = ExpandableElementContainerController();

  MapEntry<int, Widget> _buildItem(int index, dynamic micromouseResult) {
    final positionText = Text((index + 1).toString(),
        style: TextStyle(fontSize: 30.0, color: Colors.white));

    final robotName = micromouseResult['robot']['name'];
    final teamName = micromouseResult['robot']['team']['name'];
    final robotTime = ElapsedTime.fromCentiseconds(micromouseResult['score']);
    final penalty = micromouseResult['penalty'];

    return MapEntry(
      index,
      Builder(
        builder: (BuildContext context) => Card(
          elevation: 20,
          borderOnForeground: true,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 6.0,
              top: 2.0,
              right: 0.0,
              bottom: 2.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                positionText,
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 7.5),
                    height: 20.0,
                    child: VerticalDivider(color: Colors.blueGrey[200])),
                Text(
                  robotName,
                  style: TextStyle(color: Colors.white70),
                  textAlign: TextAlign.center,
                ),
                Text(
                  '  ' + robotTime.toString()+'+'+(penalty/100).toString(),
                  style: TextStyle(color: Colors.white70),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Text(
                    teamName,
                    style: TextStyle(color: Colors.white54 ),
                    textAlign: TextAlign.left,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildScoresList(BuildContext context, QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return Column(
        children: <Widget>[
          Text(
            "Ups. Coś poszło nie tak",
            style: TextStyle(
              fontSize: 30.0,
              color: Colors.white30,
            ),
          )
        ],
      );
    }
    if (result.loading) {
      return Container(
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            image: DecorationImage(
              fit: BoxFit.scaleDown,
              image: competitionImage[CompetitionCategory.MicroMouse],
            )
        ),
      );
    }
    final List scores = result.data['micromouseScoresRanking']['edges'];
    final List resultWidgets = scores
        .map((entry) => entry['node'])
        .toList()
        .asMap()
        .map(_buildItem)
        .values.toList();
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Color(0xff000719),
      ),
      child: ListView(
        children: resultWidgets,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final queryOptions = QueryOptions(
      document: MICROMOUSE_SCORELIST,
      pollInterval: 10,
    );
    Icon backIcon;

    if (Theme.of(context).platform == TargetPlatform.iOS) {
      backIcon = Icon(Icons.arrow_back_ios);
    } else {
      backIcon = Icon(Icons.arrow_back);
    }

    return Scaffold(
      appBar: AppBar(
          title: Text("Micromouse"),
          leading: IconButton(
            icon: backIcon,
            onPressed:() => Navigator.pop(context, false),
          )
      ),
      drawer: NavigationDrawer(),
      backgroundColor: Color(0xff1b1e29),
      body: Theme(
        data: Theme.of(context).copyWith(
          cardColor: Colors.white10,
        ),
        child: ExpandableElementContainer(
          controller: _controller,
          child: Builder(
            builder:(context) => Query(
              options: queryOptions,
              builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                return _buildScoresList(context, result, refetch: refetch, fetchMore: fetchMore);
              },
            ),
          ),
        ),
      ),
    );
  }
}
