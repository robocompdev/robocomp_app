import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/competition_types.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';
import 'package:robocomp_app/graphql_documents/query/linefollower.dart';

enum Phase {
  QUALIFICATION,
  FINAL
}

class LinefollowerPhaseSubpage extends StatelessWidget {
  final CompetitionCategory category;
  final Phase phase;

  LinefollowerPhaseSubpage({
    @required this.category,
    @required this.phase,
  });

  MapEntry<int, Widget> _buildItem(int index, dynamic linefollowerResult) {
    final positionText = Text((index + 1).toString(),
        style: TextStyle(fontSize: 30.0, color: Colors.white));

    final robotName = linefollowerResult['robot']['name'];
    final teamName = linefollowerResult['robot']['team']['name'];
    final time = ElapsedTime.fromCentiseconds(linefollowerResult['score']);

    return MapEntry(
      index,
      Builder(
        builder: (BuildContext context) => Card(
          elevation: 20,
          borderOnForeground: true,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 6.0,
              top: 2.0,
              right: 0.0,
              bottom: 2.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                positionText,
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 7.5),
                    height: 20.0,
                    child: VerticalDivider(color: Colors.blueGrey[200])),
                Text(
                  robotName,
                  style: TextStyle(color: Colors.white70),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "    "+time.toString(),
                  style: TextStyle(color: Colors.white70),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Text(
                    teamName,
                    style: TextStyle(color: Colors.white54 ),
                    textAlign: TextAlign.left,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildScoresList(BuildContext context, QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return Column(
        children: <Widget>[
          Text(
            "Ups. Coś poszło nie tak",
            style: TextStyle(
              fontSize: 30.0,
              color: Colors.white30,
            ),
          )
        ],
      );
    }
    if (result.loading) {
      return Container(
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            image: DecorationImage(
              fit: BoxFit.scaleDown,
              image: competitionImage[category],
            )
        ),
      );
    }
    List scores;
    switch(phase) {
      case Phase.FINAL:
        scores = result.data['linefollowerScoreFinalRanking']['edges'];
        break;
      case Phase.QUALIFICATION:
        scores = result.data['linefollowerScoreQualificationRanking']['edges'];
        break;
    }

    final List resultWidgets = scores
        .map((entry) => entry['node'])
        .toList()
        .asMap()
        .map(_buildItem)
        .values.toList();
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Color(0xff000719),
      ),
      child: ListView(
        children: resultWidgets,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String document;
    switch(phase) {
      case Phase.FINAL:
        document = LINEFOLLOWER_FINAL_PHASE_RESULTS;
        break;
      case Phase.QUALIFICATION:
        document = LINEFOLLOWER_QUALIFICATION_PHASE_RESULTS;
        break;
    }
    final queryOptions = QueryOptions(
      document: document,
      variables: {
        'linefollowerType': backendCompetitionNames[category],
      },
      pollInterval: 10,
    );
    return Query(
      options: queryOptions,
      builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
        return _buildScoresList(context, result, refetch: refetch, fetchMore: fetchMore);
      },
    );
  }
}
