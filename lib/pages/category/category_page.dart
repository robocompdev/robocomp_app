import 'package:flutter/material.dart';
import 'package:robocomp_app/generated/i18n.dart';
import 'package:robocomp_app/pages/category/category_list.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text(S
            .of(context)
            .categories),
      ),
      drawer: NavigationDrawer(),
      body: CategoryList(),
    );
  }
}
