import 'package:flutter/material.dart';
import 'package:robocomp_app/competition_types.dart';


final defaultImage = AssetImage("assets/sumo.png");

class CategoryList extends StatefulWidget {
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  Widget _buildCategoryItem(CompetitionCategory type) {
    final categoryImage = Container(
      width: 75.0,
      height: 75.0,
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          image: DecorationImage(
            fit: BoxFit.scaleDown,
            image: competitionImage[type],
          )
      ),
    );

    String routeDestination;

    switch (competitionTypeMap[type]) {
      case CompetitionType.MICROMOUSE:
        routeDestination = '/micromouse_results_view';
        break;
      case CompetitionType.LINEFOLLOWER:
        routeDestination = "/linefollower_results_page";
        break;
      case CompetitionType.BEARRESCUE:
        routeDestination = '/bear_rescue_results_view';
        break;
      case CompetitionType.FREESTYLE:
        routeDestination = '/freestyle_results_page';
        break;

      default:
        routeDestination = "/";
    }

    final routeButton = Container(
      alignment: Alignment.centerRight,
      child: IconButton(
        onPressed: () {
          Navigator.of(context).pushNamed(routeDestination, arguments: type);
        },
        color: Color(0x4360ffbb),
        icon: Icon(Icons.forward),
      ),
    );

    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(routeDestination, arguments: type);
      },
      child: Theme(
        data: ThemeData(
            cardColor: Colors.white10
        ),
        child: Card(
          elevation: 20,
          borderOnForeground: true,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 6.0,
              top: 2.0,
              right: 0.0,
              bottom: 2.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                categoryImage,
                Expanded(
                  child: Text(
                    competitionNames[type],
                    style: TextStyle(color: Colors.white70, fontSize: 16.0),
                    textAlign: TextAlign.center,
                  ),
                ),
                //mapButton,
                routeButton
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context) {
    return ListView(
      children: CompetitionCategory.values.map(_buildCategoryItem).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }
}
