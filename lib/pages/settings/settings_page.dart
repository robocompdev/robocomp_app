import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

enum SUPPORTED_LANGUAGES { PL, EN, PL_KN }

const Map<SUPPORTED_LANGUAGES, String> languageName = {
  SUPPORTED_LANGUAGES.PL: "Polski",
  SUPPORTED_LANGUAGES.EN: "Angielski",
  SUPPORTED_LANGUAGES.PL_KN: "Kaszubski",
};

class LanguagePopup extends StatefulWidget {
  @override
  _LanguagePopupState createState() => _LanguagePopupState();
}

class _LanguagePopupState extends State<LanguagePopup> {
  SUPPORTED_LANGUAGES _currentLanguage = SUPPORTED_LANGUAGES.PL;

  Widget _buildItem(SUPPORTED_LANGUAGES language) {
    final positionText = Text(
      languageName[language],
      style: TextStyle(
        fontSize: 30.0,
        color: Colors.white,
      ),
    );
    bool _value = language == _currentLanguage;
    return Card(
      color: Color(0xff1b1e29),
      child: Container(
        decoration: ShapeDecoration(
          color: Colors.white10,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0)),
        ),
        child: CheckboxListTile(
          activeColor: Colors.indigo,
          title: positionText,
          value: _value,
          onChanged: (bool value) {
            setState(() {
              _currentLanguage = language;
            });
          },
        ),
      ),
    );
  }

  List<Widget> _buildList(BuildContext context) {
    List<Widget> _builtWidgets = [];
    SUPPORTED_LANGUAGES.values
        .forEach((language) {
      _builtWidgets.add(_buildItem(language));
    },
    );
    return _builtWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      backgroundColor: Color(0xff1b1e29),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 10.0),
        height: 212.5,
        width: 300,
        child: ListView(
          children: _buildList(context),
        ),
      ),
    );
  }
}

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _notifications = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text("Ustawienia"),
      ),
      body: Column(
        children: <Widget>[
          Card(
            color: Colors.white10,
            elevation: 20.0,
            child: SizedBox(
              height: 55.0,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return LanguagePopup();
                      }
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 30.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Wybierz język",
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                          Icon(
                            Icons.translate,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            color: Colors.white10,
            elevation: 20.0,
            child: SwitchListTile(
              title: Text(
                "Powiadomienia",
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
              value: _notifications,
              onChanged: (bool state) {
                setState(() {
                  _notifications = !_notifications;
                });
              },
            ),
          ),
          Card(
            color: Colors.white10,
            elevation: 20.0,
            child: SizedBox(
              height: 55.0,
              child: GestureDetector(
                onTap: () { //                                TODO
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 30.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Ustawienia profilu",
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                          Icon(
                            Icons.child_care,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
