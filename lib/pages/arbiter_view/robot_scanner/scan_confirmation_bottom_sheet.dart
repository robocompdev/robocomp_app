import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:robocomp_app/generated/i18n.dart';

class ScanConfirmationBottomSheet extends StatelessWidget {
  const ScanConfirmationBottomSheet({
    Key key,
    @required QRViewController controller,
    this.scannedCode,
  })  : _controller = controller,
        super(key: key);

  final QRViewController _controller;
  final String scannedCode;

  @override
  Widget build(BuildContext context) {
    //todo: parsing data
    return Container(
      height: 170,
      padding: EdgeInsets.only(
        top: 20.0,
        bottom: 20.0,
      ),
      color: Color(0xff000719),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Align(
              alignment: Alignment.topLeft,
              child: RichText(
                text: TextSpan(
                  text: S.of(context).robot_id,
                  style: TextStyle(color: Colors.white54),
                  children: [
                    TextSpan(
                      text: scannedCode,
                      style: TextStyle(color: Colors.white54),
                    ),
                  ]
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 30.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  child: Text(S.of(context).retry),
                  color: Colors.redAccent,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text(S.of(context).confirm),
                  color: Colors.green,
                  onPressed: () {
                    Navigator.of(context).pop<bool>(true);
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
