import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';
import 'package:robocomp_app/pages/arbiter_view/robot_scanner/scan_confirmation_bottom_sheet.dart';

class RobotScanner extends StatefulWidget {
  RobotScanner({Key key}) : super(key: key);
  final GlobalKey _qrKey = GlobalKey(debugLabel: 'QR');

  @override
  _RobotScannerState createState() => _RobotScannerState();
}

class _RobotScannerState extends State<RobotScanner> {
  QRViewController _controller;
  bool _flashlightEnabled;

  void _onQrViewCreated(BuildContext context, QRViewController controller) {
    _controller = controller;
    _controller.scannedDataStream.listen((scanData) {
      controller.pauseCamera();
      showModalBottomSheet<bool>(
          context: context,
          backgroundColor: Colors.white,
          builder: (context) => ScanConfirmationBottomSheet(
            controller: _controller,
            scannedCode: scanData,
          )).then((accept) {
        if(accept == null || !accept) {
          _controller.resumeCamera();
        } else {
          Navigator.of(context).pop<String>(scanData);
        }
      });
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _flashlightEnabled = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Robot scanner'),
      ),
      backgroundColor: Color(0xff000719),
      body: Stack(
        fit: StackFit.expand,
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            child: Builder(
              builder: (context) => QRView(
                key: widget._qrKey,
                onQRViewCreated: (controller) {
                  _onQrViewCreated(context, controller);
                },
                overlay: QrScannerOverlayShape(
                  borderColor: Colors.red,
                  borderRadius: 10,
                  borderLength: 30,
                  borderWidth: 10,
                  cutOutSize: 300,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 25.0,
              right: 25.0,
              top: 30.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: RawMaterialButton(
                    shape: CircleBorder(),
                    elevation: 0.0,
                    child: Icon(
                      Icons.lightbulb_outline,
                      color: _flashlightEnabled? Colors.amberAccent : Colors.white54,
                    ),
                    onPressed: () {
                      _controller.toggleFlash();
                      setState(() {
                        _flashlightEnabled = !_flashlightEnabled;
                      });
                    },
                    fillColor: Color(0xff820d35),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: RawMaterialButton(
                    shape: CircleBorder(),
                    elevation: 0.0,
                    child: Icon(
                      Icons.switch_camera,
                      color: Colors.white54,
                    ),
                    onPressed: () {
                      _controller.flipCamera();
                    },
                    fillColor: Color(0xff820d35),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
