import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';

class CircularStopwatch extends StatefulWidget {
  final double size;
  final CircularStopwatchController _controller;
  final Color ringBackgroundColor;
  final Color ringActiveColor;
  final Color centerColor;
  final bool showPenalty;

  CircularStopwatch({
    Key key,
    @required this.size,
    CircularStopwatchController controller,
    this.ringBackgroundColor = Colors.amber,
    this.ringActiveColor = Colors.red,
    this.centerColor = const Color(0xff820d35),
    @required this.showPenalty,
  }) :
        _controller = controller ?? CircularStopwatchController(),
        super(key: key);
  
  @override
  _CircularStopwatchState createState() => _CircularStopwatchState();
}

class _CircularStopwatchState extends State<CircularStopwatch>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Timer _timer;
  int _milliseconds;

  void _onTick(Timer timer) {
    if(_milliseconds != widget._controller._stopwatch.elapsedMilliseconds) {
      _milliseconds = widget._controller._stopwatch.elapsedMilliseconds;
      final int centiseconds = (_milliseconds / 10).truncate()%100;
      final int seconds = (_milliseconds/1000).truncate()%60;
      final int minutes = (_milliseconds/60000).truncate();
      widget._controller._elapsedMinutesSecondsStream.add(ElapsedMinutesSeconds(
          minutes: minutes,
          seconds: seconds,
        )
      );
      widget._controller._elapsedCentisecondsStream.add(centiseconds);
    }
  }

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
    );
    _milliseconds = 0;
    _timer = Timer.periodic(
      Duration(milliseconds: widget._controller.millisecondsRefreshRate),
      _onTick,
    );
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _timer?.cancel();
    _timer = null;
    widget._controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size,
      height: widget.size,
      child: StreamBuilder<ElapsedMinutesSeconds>(
        stream: widget._controller._elapsedMinutesSecondsStream.stream,
        builder: (context, snapshot) {
          int seconds = 0;
          int minutes = 0;
          if(snapshot.hasData) {
            seconds = snapshot.data.seconds;
            minutes = snapshot.data.minutes;
          }
          return CustomPaint(
            foregroundPainter: CircularProgressPainter(
              backgroundLineColor: widget.ringBackgroundColor,
              progress: seconds%60/60.0,
              secondsLineColor: widget.ringActiveColor,
              strokeWidth: 5.0,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: ShapeDecoration(
                  color: widget.centerColor,
                  shape: CircleBorder(),
                ),
                child: DefaultTextStyle(
                  style: TextStyle(fontSize: 60),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(minutes.toString().padLeft(2, '0')),
                          Text(':'),
                          Text(seconds.toString().padLeft(2, '0')),
                          Text(':'),
                          StreamBuilder<int>(
                            stream: widget._controller._elapsedCentisecondsStream.stream,
                            initialData: 0,
                            builder: (context, snapshot) {
                                return Text(snapshot.data.toString().padLeft(2, '0'));
                              }
                          )
                        ],
                      ),
                      Visibility(
                        visible: widget.showPenalty,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            StreamBuilder<int>(
                              stream: widget._controller._penaltyStream.stream,
                              initialData: 0,
                              builder: (context, snapshot) {
                                return Text(
                                  "+ " + snapshot.data.toString().padLeft(2, '0') + ":00",
                                  style: TextStyle(fontSize: 20.0),
                                );
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        }
      ),
    );
  }
}

class CircularStopwatchController {
  final Stopwatch _stopwatch = Stopwatch();
  final _elapsedMinutesSecondsStream = StreamController<ElapsedMinutesSeconds>();
  final _elapsedCentisecondsStream = StreamController<int>();
  final _penaltyStream = StreamController<int>();
  final int millisecondsRefreshRate;
  int _penalty = 0;

  CircularStopwatchController({
    this.millisecondsRefreshRate=30,
  });

  void dispose() {
    _elapsedMinutesSecondsStream.close();
    _elapsedCentisecondsStream.close();
    _penaltyStream.close();
  }

  void pause() {
    _stopwatch.stop();
  }

  void start() {
    _stopwatch.start();
  }

  void reset() {
    _penalty = 0;
    _penaltyStream.add(_penalty);
    _stopwatch.reset();
  }

  bool isRunning() {
    return _stopwatch.isRunning;
  }

  void addPenalty(penalty) {
    _penalty += penalty;
    _penaltyStream.add(_penalty);
  }

  int get penalty => _penalty;

  ElapsedTime elapsedTime() {
    final int milliseconds = _stopwatch.elapsedMilliseconds;
    final int centiseconds = (milliseconds / 10).truncate()%100;
    final int seconds = (milliseconds/1000).truncate()%60;
    final int minutes = (milliseconds/60000).truncate();
    return ElapsedTime(
      centiseconds: centiseconds,
      seconds: seconds,
      minutes: minutes,
    );
  }
}

class CircularProgressPainter extends CustomPainter {
  final Color backgroundLineColor;
  final Color secondsLineColor;
  final double strokeWidth;
  final double progress;

  CircularProgressPainter({
    @required this.backgroundLineColor,
    this.secondsLineColor,
    this.strokeWidth,
    this.progress,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final backgroundLine = Paint()
      ..color = backgroundLineColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;

    final secondsLine = Paint()
    ..color = secondsLineColor
    ..strokeCap = StrokeCap.round
    ..style = PaintingStyle.stroke
    ..strokeWidth = strokeWidth;

    final center = Offset(size.width/2, size.height/2);
    final secondRadius = min(size.width/2,size.height/2);

    canvas.drawCircle(center, secondRadius, backgroundLine);
    final secondsFillAngle = 2*pi*progress;

    canvas.drawArc(
        Rect.fromCircle(
            center: center,
            radius: secondRadius
        ),
        -pi/2,
        secondsFillAngle,
        false,
        secondsLine);
  }

  @override
  bool shouldRepaint(CircularProgressPainter oldDelegate) =>
      backgroundLineColor != oldDelegate.backgroundLineColor ||
      secondsLineColor != oldDelegate.secondsLineColor ||
      strokeWidth != oldDelegate.strokeWidth ||
      progress != oldDelegate.progress;
}