import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/custom_widgets/shimmer_skeletal_text/shimmer_skeletal_text.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';
import 'package:robocomp_app/competition_types.dart';

class CategoryData {
  String categoryId;
  String categoryName;
  CompetitionCategory categoryEnum;

  CategoryData({
    @required this.categoryId,
    @required this.categoryName,
    @required this.categoryEnum,
  });
}

class ArbiterPage extends StatefulWidget {
  @override
  _ArbiterPageState createState() => _ArbiterPageState();
}

class _ArbiterPageState extends State<ArbiterPage> {
  void _buildCategoryDialog(BuildContext context, name) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return Query(
            options: QueryOptions(
              document: competitionQueries[name],
            ),
            builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
              if(result.hasException) {
                return SimpleDialog(
                  title: Text("Ups"),
                  children: <Widget>[
                    Center(
                        child: Text("Coś poszło nie tak")
                    ),
                  ],
                );
              }
              if (!result.loading) {
                List<Widget> competitions = [];
                result.data[competitionQueryName[name]]['edges'].forEach((category) {
                  competitions.add(
                    SimpleDialogOption(
                      child: Text(category['node']['name']),
                      onPressed: () {
                        CategoryData args = CategoryData(
                          categoryId: category['node']['id'],
                          categoryName: category['node']['name'],
                          categoryEnum: competitionEnums[name],
                        );
                        Navigator.of(context).pop(args);
                      },
                    ),
                  );
                });
                return SimpleDialog(
                  title: Text("Choose wisely"),
                  children: competitions.isEmpty
                      ? [Center(
                          child: Text("Brak zarejestrowanych kategorii"),
                        ),]
                      : competitions,
                );
              }
              else {
                return ShimmerSkeletalText(
                  backgroundColor: Colors.white24,
                  gradientColor: Colors.white30,
                );
              }
            }
        );
      }
    ).then((args){
      if(args != null) {
        Navigator.of(context).pushNamed(
          '/race_arbiter_page',
          arguments: args,
        );
      }
    });
  }

  List<Widget> _buildCategorySubpagesButtons(BuildContext context) {
    return competitionRoutes.map((String name, String route) { //TODO route nie jest używane nigdzie
      return MapEntry(                                         //TODO competitionRoutes jest używane tylko w tym pliku
          name,
          MaterialButton(
            padding: EdgeInsets.all(15.0),
            color: Colors.white54,
            onPressed: () {
              _buildCategoryDialog(context, name);
            },
            child: Text(name),
          )
      );
    }).values.toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text("Strona sędziego"),
      ),
      drawer: NavigationDrawer(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 100.0, vertical: 100.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _buildCategorySubpagesButtons(context),
          ),
        ),
      ),
    );
  }
}
