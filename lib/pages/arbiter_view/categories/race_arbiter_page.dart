import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/competition_types.dart';
import 'package:robocomp_app/custom_widgets/search_field/search_popup_field.dart';
import 'package:robocomp_app/custom_widgets/shimmer_skeletal_text/shimmer_skeletal_text.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';
import 'package:robocomp_app/graphql_documents/mutations/results.dart';
import 'package:robocomp_app/pages/arbiter_view/arbiter_page.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/beerrescue.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/micromouse/micromouse_stopwatch.dart';
import 'package:robocomp_app/graphql_documents/query/robot.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';

const String NO_RESULTS = "\u2013\u2013:\u2013\u2013:\u2013\u2013";

const Map<CompetitionCategory,String> mutationDocuments = {
  CompetitionCategory.Linefollower: ADD_LINEFOLLOWER_SCORE,
  CompetitionCategory.MicroMouse: ADD_MICROMOUSE_SCORE,
  CompetitionCategory.BearRescue: ADD_BEARRESCUE_SCORE,
};

const Map<CompetitionCategory,String> resultPage = {
  CompetitionCategory.Linefollower: '/linefollower_stopwatch',
  CompetitionCategory.MicroMouse: '/micromouse_stopwatch',
  CompetitionCategory.BearRescue: '/bearrescue_stopwatch',
};

class RaceArbiterPage extends StatefulWidget {
  @override
  _RaceArbiterPageState createState() => _RaceArbiterPageState();
}

class _RaceArbiterPageState extends State<RaceArbiterPage> {
  bool _robotSelected = false;
  String _robotName = " ";
  String _robotId;
  String _teamName = " ";
  String _runtime = NO_RESULTS;
  String _totalTime = NO_RESULTS;
  int _penalty = 0;
  int _result;

  @override
  Widget build(BuildContext context) {
    final CategoryData categoryData = ModalRoute.of(context).settings.arguments;
    final categoryTile = Card(
      child: DefaultTextStyle(
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.black,
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Kategoria: " + competitionNames[categoryData.categoryEnum]),
                ),
              ],
            ),
          ],
        ),
      ),
    );

    final robotTile = Card(
      child: DefaultTextStyle(
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.black,
        ),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Dane robota\n",
                    style: TextStyle(
                      fontSize: 24.0,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Robot:"),
                      Text(_robotName),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Drużyna:"),
                      Text(_teamName),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: RawMaterialButton(
                onPressed: (){
                  setState(() {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return SimpleDialog(
                          children: <Widget>[
                            Icon(Icons.warning),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                                    child: Center(
                                      child: Text(
                                        "Usunie to wszystkie dane obecnego robota!",
                                        style: TextStyle(fontSize: 16.0),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      RaisedButton(
                                        child: Text("Ok"),
                                        onPressed: (){
                                          setState(() {
                                            _robotSelected = false;
                                            _penalty = 0;
                                            _runtime = NO_RESULTS;
                                            _totalTime = NO_RESULTS;
                                            Navigator.of(context).pop();
                                          });
                                        },
                                      ),
                                      RaisedButton(
                                        child: Text("NIE"),
                                        onPressed: (){
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        );
                      }
                    );
                  });
                },
                constraints: BoxConstraints(minWidth: 40, minHeight: 40),
                child: Icon(
                  GroovinMaterialIcons.square_edit_outline,
                  size: 30.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );

    final chooseRobotTile = Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Wybierz robota",
              style: TextStyle(fontSize: 20.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: SearchPopupField(
                    searchFieldDecoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.search,
                        size: 30.0,
                      ),
                      hintText: 'robot name',
                      contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
                    ),
                    backgroundColor: Colors.grey,
                    builder: (context, string) {
                      return Query(
                        options: QueryOptions(
                          document: SEARCH_ROBOT,
                          variables: {
                            'qName': string,
                            'qId': categoryData.categoryId,
                          },
                        ),
                        builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                          if (!result.loading) {
                            List<Widget> robots = [];
                            result.data['searchRobot']['edges'].forEach((robot) {
                              robots.add(
                                ListTile(
                                  title: Text(
                                    robot['node']['name'] + " " +
                                    robot['node']['team']['name'],
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _robotSelected = true;
                                      _robotName = robot['node']['name'];
                                      _robotId = robot['node']['id'];
                                      _teamName = robot['node']['team']['name'];
                                    });
                                  },
                                ),
                              );
                            });
                            if(robots.length == 0) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text('Brak robotów'),
                                  ),
                                ],
                              );
                            }
                            else {
                              return Scrollbar(
                                child: ListView(
                                  children: robots,
                                ),
                              );
                            }
                          }
                          else {
                            return ShimmerSkeletalText(
                              backgroundColor: Colors.white24,
                              gradientColor: Colors.white30,
                            );
                          }
                        }
                      );
                    },
                  ),
                ),
                IconButton(
                  padding: EdgeInsets.only(bottom: 5.0),
                  icon: Icon(GroovinMaterialIcons.qrcode_scan),
                  iconSize: 30.0,
                  onPressed: () {
                    Navigator.of(context).pushNamed('/robot_scanner');
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );

    final resultTile = Card(
      child: DefaultTextStyle(
        style: TextStyle(
          fontSize: 20.0,
          color: Colors.black,
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: <Widget>[
              Text(
                "Wynik\n",
                style: TextStyle(
                  fontSize: 24.0,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Najlepszy czas:"),
                  Text(_runtime),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Kara:"),
                  Text('+ ' + _penalty.toString() + ' sec.'),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Całkowity czas zadania:'),
                  Text(_totalTime),
                ],
              ),
            ],
          ),
        ),
      ),
    );

    final startRunTile = Card(
      child: RawMaterialButton(
        onPressed: (){
          if(_robotSelected) {
            Navigator.of(context).
            pushNamed(resultPage[categoryData.categoryEnum]).
            then((value){
              setState(() {
                if(value != null) {
                  switch(categoryData.categoryEnum) {
                    case CompetitionCategory.MicroMouse:
                      MicromouseResult _popResult = value;
                      _penalty = _popResult.penalty;
                      _runtime = _popResult.runtime.toString();
                      _totalTime = _popResult.totalTime.toString();
                      _result = _popResult.runtime.minutes*6000 +
                          _popResult.runtime.seconds*100 +
                          _popResult.runtime.centiseconds;
                      break;

                    case CompetitionCategory.Linefollower:
                      ElapsedTime _popResult = value;
                      _runtime = _popResult.toString();
                      _result = _popResult.minutes * 6000 +
                          _popResult.seconds * 100 +
                          _popResult.centiseconds;
                      break;

                    case CompetitionCategory.BearRescue:
                      BearRescueResult _popResult = value;
                      _totalTime = _popResult.totalTime.toString();
                      _runtime = _popResult.runtime.toString();
                      _result = _popResult.runtime.minutes*6000 +
                          _popResult.runtime.seconds*100 +
                          _popResult.runtime.centiseconds;
                      break;

                    default:
                      break;
                  }
                }
              });
            });
          }
          else showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  children: <Widget>[
                    Icon(Icons.error),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          "Wybierz robota którego chcesz sędziować!",
                          style: TextStyle(fontSize: 16.0),
                        ),
                      ),
                    ),
                  ],
                );
              }
          );
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Rozpocznij przejazd",
              style: TextStyle(fontSize: 20.0),
            ),
          ],
        ),
      ),
    );

    final saveResultsTile = Card(
      child: Mutation(
        options: MutationOptions(
          document: mutationDocuments[categoryData.categoryEnum],
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return RawMaterialButton(
            onPressed: (){
              if(_robotSelected && _runtime != NO_RESULTS) {
                switch(categoryData.categoryEnum) {
                  case CompetitionCategory.MicroMouse:
                    runMutation({
                      'mPenalty': _penalty*100,
                      'mResult': _result,
                      'mRobotId': _robotId,
                    });
                    break;

                  case CompetitionCategory.Linefollower:
                    runMutation({
                      'mIsFinal': true,
                      'mCompetitionId': categoryData.categoryId,
                      'mResult': _result,
                      'mRobotId': _robotId,
                    });
                    break;

                  case CompetitionCategory.BearRescue:
                    runMutation({
                      'mCompetitionId': categoryData.categoryId,
                      'mCompleted': true,
                      'mResult': _result,
                      'mRobotId': _robotId,
                    });
                    break;

                  default:
                }
                Navigator.of(context).pop();
              }
              else showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      children: <Widget>[
                        Icon(Icons.error),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Text(
                              "Robot nie ma wyniku!",
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ),
                        ),
                      ],
                    );
                  }
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Zapisz wyniki",
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          );
        },
      ),
    );

    final disqualifyTile = Card(
      child: Mutation(
        options: MutationOptions(
          document: DISQUALIFY_ROBOT,
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return RawMaterialButton(
            onPressed: (){
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    children: <Widget>[
                      Icon(Icons.warning),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Text(!_robotSelected ? "Wybierz robota!" :
                            "Czy jesteś pewien? \nNie będzie można tego cofnąć",
                            style: TextStyle(fontSize: 16.0),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          RaisedButton(
                            child: Text('tak'),
                            onPressed: () {
                              if(_robotSelected) runMutation({
                                'mCompetitionId': categoryData.categoryId,
                                'mRobotId': _robotId,
                              });
                              setState(() {
                                _robotSelected = false;
                              });
                              Navigator.of(context).pop();
                            },
                          ),
                          RaisedButton(
                            child:  Text(_robotSelected ? 'nie' : 'tak'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      )
                    ],
                  );
                }
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Zdyskwalifikuj robota",
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          );
        },
      ),
    );

    Icon backIcon;

    if (Theme.of(context).platform == TargetPlatform.iOS) {
      backIcon = Icon(Icons.arrow_back_ios);
    } else {
      backIcon = Icon(Icons.arrow_back);
    }

    final backButton = Builder(
        builder: (context) => IconButton(
          icon: backIcon,
          onPressed: () {
            Navigator.of(context).pop();
          },
          color: Colors.white70,
        ));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text(categoryData.categoryName),
        leading: backButton,
      ),
      drawer: NavigationDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          categoryTile,
          _robotSelected ? robotTile : chooseRobotTile,
          startRunTile,
          resultTile,
          saveResultsTile,
          disqualifyTile,
        ],
      ),
    );
  }
}
