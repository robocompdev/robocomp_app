import 'package:flutter/material.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';
import 'package:robocomp_app/pages/arbiter_view/stopwatch/circular_stopwatch.dart';

class LinefollowerStopwatchPage extends StatefulWidget {
  @override
  _LinefollowerStopwatchPageState createState() => _LinefollowerStopwatchPageState();
}

class _LinefollowerStopwatchPageState extends State<LinefollowerStopwatchPage> {
  bool _isReset = true;
  int index = 3;

  final _stopwatch = Stopwatch();
  final _controller = CircularStopwatchController();

  ElapsedTime totalTime() {
    final int milliseconds = _stopwatch.elapsedMilliseconds;
    final int seconds = (milliseconds/1000).truncate()%60;
    final int minutes = (milliseconds/60000).truncate();
    return ElapsedTime(
      minutes: minutes,
      seconds: seconds,
      centiseconds: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text("Linefollower"),
          leading: IconButton(
            icon: Icon(
              Theme.of(context).platform == TargetPlatform.iOS ?
                Icons.arrow_back_ios : Icons.arrow_back,
            ),
            onPressed:() => Navigator.of(context).pop(),
          )
      ),
      drawer: NavigationDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          CircularStopwatch(
            size: 300.0,
            controller: _controller,
            showPenalty: false,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0, bottom: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RawMaterialButton(
                  fillColor: _isReset ?
                  Colors.lightGreen :
                  (
                      _controller.isRunning() ? Colors.grey : Colors.yellow
                  ),
                  shape: CircleBorder(),
                  child: Icon(
                    _isReset ? Icons.play_arrow : Icons.refresh,
                    color: Colors.white,
                    size: 40.0,
                  ),
                  padding: EdgeInsets.all(20.0),
                  onPressed: (){
                    setState(() {
                      if(!_controller.isRunning()) {
                        if (_isReset) {
                          _controller.start();
                          _isReset = false;
                        }
                        else {
                          index = 3;
                          _controller.reset();
                          _isReset = true;
                        }
                      }
                    });
                  },
                ),
                RawMaterialButton(
                  fillColor: _controller.isRunning() ? Colors.redAccent : Colors.blueAccent,
                  shape: CircleBorder(),
                  child: Icon(
                    _controller.isRunning() ? Icons.stop : Icons.save,
                    size: 40.0,
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.all(20.0),
                  onPressed: () {
                    setState(() {
                      if(_controller.isRunning()) _controller.pause();
                      else Navigator.of(context).pop(_controller.elapsedTime());
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
