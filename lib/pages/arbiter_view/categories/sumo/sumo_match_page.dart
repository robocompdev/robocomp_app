import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:robocomp_app/custom_widgets/linear_progress_indicator/linear_progress_text_indicator.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';


const int MATCH_MAX_TIME = 18000;
const int SCORE_ADVANTAGE = 2;

enum SumoMatchState {
  NOT_STARTED,
  RUNNING,
  ENDED,
}

class TimerData {
  final ElapsedMinutesSeconds elapsedTime;
  final double percentage;

  TimerData(this.elapsedTime, this.percentage);
}

class SumoMatchPage extends StatefulWidget {
  SumoMatchPage({Key key}) : super(key: key);

  @override
  _SumoMatchPageState createState() => _SumoMatchPageState();
}

class _SumoMatchPageState extends State<SumoMatchPage> {
  Stopwatch _stopwatch;
  Timer _timer;
  final _timerStream = StreamController<TimerData>.broadcast();
  int _latestMilliseconds = 0;

  SumoMatchState state = SumoMatchState.NOT_STARTED;
  bool _timedOut = false;

  List<int> _robotScores = [0, 0];

  void _addPoint(int robotId) {
      setState(() {
        if(_robotScores[robotId] !=2) {
          ++_robotScores[robotId];
          _stopwatch.stop();
          _stopwatch.reset();
          _timedOut = false;
          state = SumoMatchState.NOT_STARTED;
          _latestMilliseconds = 0;
          _checkMatchStatus();
        }
      });
  }

  void _removePoint(int robotId) {
    setState(() {
      if(_robotScores[robotId] != 0) {
        --_robotScores[robotId];
        if(state == SumoMatchState.ENDED && !_robotScores.any((int score) => score == SCORE_ADVANTAGE)) {
          state = SumoMatchState.RUNNING;
        }
      }
    });
  }

  void _checkMatchStatus() {
    if((_robotScores.any((int score) => score == SCORE_ADVANTAGE))) {
      state = SumoMatchState.ENDED;
    }
  }

  void _matchDraw() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ostrzeżenie"),
            actions: <Widget>[
              FlatButton(
                child: Text("Anuluj"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text("Potwierdź"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        }
    ).then((result) {

    });
  }
  
  Widget _buildTimerButton() {
    if(_timedOut) {
      return MaterialButton(
        onPressed: () {
          setState(() {
            _stopwatch.stop();
            _stopwatch.reset();
            _timedOut = false;
            state = SumoMatchState.NOT_STARTED;
            _latestMilliseconds = 0;
          });
        },
        color: Colors.red,
        padding: const EdgeInsets.all(13.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Text(
          "Remis",
          style: TextStyle(fontSize: 20),
        ),
      );
    }
    if(state == SumoMatchState.NOT_STARTED) {
      return MaterialButton(
        onPressed: () {
          _stopwatch.start();
          setState(() {
            state = SumoMatchState.RUNNING;
          });
        },
        color: Colors.red,
        padding: const EdgeInsets.all(13.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Text(
          "Rozpocznij walkę",
          style: TextStyle(fontSize: 20),
        ),
      );
    } else if(state == SumoMatchState.RUNNING){
      return StreamBuilder<TimerData>(
        stream: _timerStream.stream,
        initialData: TimerData(
          ElapsedMinutesSeconds(seconds: 0, minutes: 3),
          0.0,
        ),
        builder: (context, snapshot) => LinearProgressTextIndicator(
          lineWidth: 40.0,
          progress: snapshot.data.percentage,
          backgroundColor: Colors.green,
          child: Text(
            snapshot.data.elapsedTime.toString(),
            style: TextStyle(
              fontSize: 30.0
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void _onTick(Timer timer) {
    if(_stopwatch.elapsedMilliseconds > _latestMilliseconds) {
      _latestMilliseconds = _stopwatch.elapsedMilliseconds;
      final int milliseconds = MATCH_MAX_TIME - _latestMilliseconds;
      final int seconds = milliseconds ~/ 1000 % 60;
      final int minutes = milliseconds ~/ 60000;
      final double percentage = _latestMilliseconds / MATCH_MAX_TIME;
      debugPrint(seconds.toString());

      _timerStream.add(
          TimerData(
              ElapsedMinutesSeconds(
                minutes: minutes,
                seconds: seconds,
              ),
              percentage
          )
      );

      if (seconds == 0 && minutes == 0) {
        _stopwatch.stop();
        setState(() {
          _timedOut = true;
        });
      }
    }
  }

  @override void initState() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]
    );
    _stopwatch = Stopwatch();
    _timer = Timer.periodic(
      Duration(milliseconds: 500),
      _onTick,
    );
    super.initState();
  }

  @override void dispose() {
    _timerStream.close();
    _timer?.cancel();
    _timer = null;
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]
    );

    Icon backIcon;
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      backIcon = Icon(Icons.arrow_back_ios);
    } else {
      backIcon = Icon(Icons.arrow_back);
    }

    final backButton = Builder(
        builder: (context) => IconButton(
          icon: backIcon,
          onPressed: () {
            SystemChrome.setPreferredOrientations(
                [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]
            ).then((_) {
              Navigator.of(context).pop();
            });
          },
          color: Colors.white70,
        )
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Sumo match"),
        leading: backButton,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              GroovinMaterialIcons.flag,
              color: Colors.white,
            ),
            onPressed: _matchDraw,
          )
        ],
      ),
      backgroundColor: Color(0xff1b1e29),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                      flex: 4,
                      child: SumoPointField(value: _robotScores[0],)
                  ),
                  Spacer(flex: 1,),
                  Flexible(
                    flex: 4,
                    child: SumoAddButton(
                      onTap: () => _addPoint(0),
                      onLongPress: () => _removePoint(0),
                    ),
                  ),
                ]
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                flex: 5,
                child: AnimatedSwitcher(
                  duration: Duration(milliseconds: 500),
                  child: _buildTimerButton(),
                )
              ),
              Spacer(
                flex: 2,
              ),
              Flexible(
                flex: 4,
                child: AnimatedOpacity(
                  duration: Duration(seconds: 20),
                  opacity: state == SumoMatchState.ENDED ? 1.0 : 0.0,
                  child: MaterialButton(
                    onPressed: () => {},
                    color: Colors.red,
                    padding: const EdgeInsets.all(13.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      "Zakończ walkę",
                      style: TextStyle(fontSize: 20),
                    ),
                  )
                ),
              ),
            ],
          ),
          Flexible(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    flex: 4,
                    child: SumoPointField(value: _robotScores[1],)
                ),
                Spacer(flex: 1,),
                Flexible(
                  flex: 4,
                  child: SumoAddButton(
                    onTap: () => _addPoint(1),
                    onLongPress: () => _removePoint(1),
                  ),
                ),
              ]
            ),
          ),
        ],
      )
    );
  }
}

class SumoAddButton extends StatelessWidget {
  final VoidCallback onTap;
  final VoidCallback onLongPress;

  const SumoAddButton({
    Key key,
    @required this.onTap,
    @required this.onLongPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      onLongPress: onLongPress,
      child: Container(
        decoration: ShapeDecoration(
            shape: CircleBorder(),
          color: Colors.green,
        ),
        child: Icon(
          Icons.add,
          size: 70.0,
        ),
      ),
    );
  }
}

class SumoPointField extends StatelessWidget {
  final int value;
  const SumoPointField({
    Key key,
    @required this.value
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 200,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.teal.withOpacity(0.4),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Text(
          value.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white70,
            fontSize: 90
          ),
        ),
      ),
    );
  }
}