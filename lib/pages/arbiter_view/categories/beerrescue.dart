import 'dart:async';
import 'package:flutter/material.dart';
import 'package:robocomp_app/custom_widgets/utils/elapsed_time.dart';
import 'package:robocomp_app/drawer/navigation_drawer.dart';
import 'package:robocomp_app/pages/arbiter_view/stopwatch/circular_stopwatch.dart';

class BearRescueResult {
  final ElapsedTime runtime;
  final ElapsedTime totalTime;

  BearRescueResult({
    @required this.runtime,
    @required this.totalTime,
  });
}

class BearRescueStopwatchPage extends StatefulWidget {
  @override
  _BearRescueStopwatchPageState createState() => _BearRescueStopwatchPageState();
}

class _BearRescueStopwatchPageState extends State<BearRescueStopwatchPage> {
  bool _isReset = true;
  int index = 3;
  int _milliseconds;

  final _stopwatch = Stopwatch();
  Timer _timer;
  final _controller = CircularStopwatchController();
  final _timerStream = StreamController<ElapsedMinutesSeconds>();

  void _onTick(Timer timer) {
    _milliseconds = _stopwatch.elapsedMilliseconds;
    final int seconds = (_milliseconds/1000).truncate()%60;
    final int minutes = (_milliseconds/60000).truncate();
    _timerStream.add(
        ElapsedMinutesSeconds(
          minutes: minutes,
          seconds: seconds,
        )
    );
  }

  ElapsedTime totalTime() {
    final int milliseconds = _stopwatch.elapsedMilliseconds;
    final int seconds = (milliseconds/1000).truncate()%60;
    final int minutes = (milliseconds/60000).truncate();
    return ElapsedTime(
      minutes: minutes,
      seconds: seconds,
      centiseconds: 0,
    );
  }

  @override
  void initState() {
    _milliseconds = 0;
    _stopwatch.start();
    _timer = Timer.periodic(
      Duration(milliseconds: 1000),
      _onTick,
    );
    super.initState();
  }

  @override
  void dispose() {
    _timerStream.close();
    _timer?.cancel();
    _timer = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1b1e29),
      appBar: AppBar(
        title: Text("Bear Rescue"),
        leading: IconButton(
          icon: Icon(
            Theme.of(context).platform == TargetPlatform.iOS ?
            Icons.arrow_back_ios : Icons.arrow_back,
          ),
          onPressed:() => Navigator.of(context).pop(),
        )
      ),
      drawer: NavigationDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                StreamBuilder<ElapsedMinutesSeconds>(
                  stream: _timerStream.stream,
                  builder: (context, snapshot) {
                    int seconds = 0;
                    int minutes = 0;
                    if(snapshot.hasData) {
                      seconds = snapshot.data.seconds;
                      minutes = snapshot.data.minutes;
                    }
                    return Container(
                      decoration: ShapeDecoration(
                        color: Colors.redAccent,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                      ),
                      child: DefaultTextStyle(
                        style: TextStyle(fontSize: 30.0),
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(minutes.toString().padLeft(2, '0')),
                                  Text(':'),
                                  Text(seconds.toString().padLeft(2, '0')),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          CircularStopwatch(
            size: 300.0,
            controller: _controller,
            showPenalty: false,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0, bottom: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RawMaterialButton(
                  fillColor: _isReset ?
                  Colors.lightGreen :
                  ( _controller.isRunning() ? Colors.grey : Colors.yellow ),
                  shape: CircleBorder(),
                  child: Icon(
                    _isReset ? Icons.play_arrow : Icons.refresh,
                    color: Colors.white,
                    size: 40.0,
                  ),
                  padding: EdgeInsets.all(20.0),
                  onPressed: (){
                    setState(() {
                      if(!_controller.isRunning()) {
                        if (_isReset) {
                          _controller.start();
                          _isReset = false;
                        }
                        else {
                          index = 3;
                          _controller.reset();
                          _isReset = true;
                        }
                      }
                    });
                  },
                ),
                RawMaterialButton(
                  fillColor: _controller.isRunning() ? Colors.redAccent : Colors.blueAccent,
                  shape: CircleBorder(),
                  child: Icon(
                    _controller.isRunning() ? Icons.stop : Icons.save,
                    size: 40.0,
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.all(20.0),
                  onPressed: () {
                    setState(() {
                      if(_controller.isRunning()) {
                        _controller.pause();
                      }
                      else sendResult(_controller.penalty, _controller.elapsedTime(), totalTime());
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  void sendResult(penalty, runtime, totalTime){
    Navigator.of(context).pop(
      BearRescueResult(
        runtime: runtime,
        totalTime: totalTime,
      ),
    );
  }
}
