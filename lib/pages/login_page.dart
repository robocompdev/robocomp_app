import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:robocomp_app/custom_widgets/progress_button/progress_button.dart';
import 'package:robocomp_app/generated/i18n.dart';
import 'package:robocomp_app/models/authentication_state.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameFieldController = TextEditingController();
  final _passwordFieldController = TextEditingController();
  final _usernameFocus = FocusNode();
  final _passwordFocus = FocusNode();
  final _signInButtonController = ProgressButtonController();
  final _formKey = GlobalKey<FormState>();

  Future<bool> _signIn() async {
    final username = _usernameFieldController.value.text;
    final password = _passwordFieldController.value.text;
    return await Provider.of<AuthenticationState>(context, listen: false)
        .signIn(username, password);
  }

  Future<bool> _signInAsGuest() async {
    Provider.of<AuthenticationState>(context, listen: false).signInAsGuest();
    return true;
  }

  void _showFailure(BuildContext context) {
      final scaffold = Scaffold.of(context);
      scaffold.showSnackBar(
        SnackBar(
          content: Text(S.of(context).invalid_username_or_password),
          duration: Duration(seconds: 3),
          backgroundColor: Colors.white10,
        ),
      );
  }

  String _validateUsername(String username) {
    if(username.isEmpty) {
      return S.of(context).username_cant_be_blank;
    }
    return null;
  }

  String _validatePassword(String username) {
    if(username.length<4) {
      return S.of(context).password_must_have_at_least_4_characters;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    final usernameField = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      controller: _usernameFieldController,
      textInputAction: TextInputAction.next,
      focusNode: _usernameFocus,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        hintText: 'Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (term) {
        _usernameFocus.unfocus();
        FocusScope.of(context).requestFocus(_passwordFocus);
      },
      validator: _validateUsername,
    );

    final passwordField = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      obscureText: true,
      controller: _passwordFieldController,
      textInputAction: TextInputAction.done,
      focusNode: _passwordFocus,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (term) {
        _passwordFocus.unfocus();
        _signInButtonController.run();
      },
      validator: _validatePassword,
    );

    final signInButton = Builder(
      builder: (BuildContext context) {
        return ProgressButton(
          idleChild: Text(
              S.of(context).sign_in,
              style: TextStyle(color: Colors.white)
          ),
          duration: Duration(milliseconds: 500),
          minWidth: 45.0,
          activeChild: SizedBox(
            width: 29.0,
            height: 29.0,
            child: CircularProgressIndicator(
              backgroundColor: Colors.grey,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white70),
            ),
          ),
          failureChild: Icon(Icons.cancel, size: 20.0,color: Colors.white,),
          successChild: Icon(Icons.check, size: 20.0,color: Colors.white,),
          actionDelay: Duration(seconds: 3),
          onFailure: (){
            _showFailure(context);
          },
          onClick: () async {
            if(!_formKey.currentState.validate()) return false;
            return await _signIn();
          },
          onSuccess: (){
            Navigator.of(context).pushReplacementNamed('/category_page');
          },
          color: Color(0xff820d35),
          progressButtonController: _signInButtonController,
        );
      },
    );

    final guestButton = ProgressButton(
      idleChild: Text(
          S.of(context).sign_in_as_guest,
          style: TextStyle(color: Colors.white)
      ),
      duration: Duration(milliseconds: 500),
      minWidth: 45.0,
      activeChild: SizedBox(
        width: 29.0,
        height: 29.0,
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white70),
        ),
      ),
      failureChild: Icon(Icons.cancel, size: 20.0,color: Colors.white,),
      successChild: Icon(Icons.check, size: 20.0,color: Colors.white,),
      actionDelay: Duration(seconds: 3),
      onFailure: (){},
      onClick: () async {
        return await _signInAsGuest();
      },
      onSuccess: (){
        Navigator.of(context).pushReplacementNamed('/category_page');
      },
      color: Color(0xff820d35),
    );

    final logo = Hero(
        tag: 'logo',
        child: Image.asset("assets/robocomp_logo.png")
    );

    Widget loginForm = Form(
      key: _formKey,
      child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 70.0),
            logo,
            SizedBox(height: 30.0),
            usernameField,
            SizedBox(height: 10.0),
            passwordField,
            SizedBox(height: 25.0),
            signInButton,
            SizedBox(height: 10.0),
            guestButton,
          ],
      ),
    );

    return Scaffold(
      backgroundColor: Color(0xff000719),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: loginForm,
          ),
        ],
      ),
    );
  }
}
