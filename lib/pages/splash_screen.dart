import 'dart:math';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:robocomp_app/models/authentication_state.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;
  Future<void> _checkSignIn(BuildContext context) async {
    if (await Provider.of<AuthenticationState>(context, listen: false).restoreLocalAuthToken() == true)
    {
      await animationController.forward();
      Navigator.of(context).pushReplacementNamed('/category_view');
    }
    else {
      await animationController.forward();
      Navigator.of(context).pushReplacementNamed('/login_page');
    }
  }

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
        duration: Duration(milliseconds: 5000), vsync: this);
    animation = Tween(begin: 0.1, end: 0.9).animate(animationController);

    WidgetsBinding.instance.addPostFrameCallback((_){
      _checkSignIn(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    return LogoAnimation(
      animation: animation,
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}

class LogoAnimation extends AnimatedWidget {
  LogoAnimation({Key key, Animation animation})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {

    Animation animation = listenable;
    bool state;
    double AmountOpa;
    var showText;

    if(animation.value<0.45)
      AmountOpa= 1- 1.4*animation.value;
    else {
      AmountOpa = (-0.40 + animation.value) * 2;
      if(AmountOpa>1)
        AmountOpa=1;
    }

    if(animation.value<0.45)
      showText="Tip dnia";
    else
      showText = string;


    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: Stack(
        children: <Widget>[
          Center(
            child: Container(
              height: 400-animation.value*400,
              width: 400-animation.value*400,
              transform: Matrix4.translationValues(0, animation.value*800-600, 0),
              child: Hero(
                tag: "robot",
                child: Opacity(
                  opacity: 1-animation.value,
                  child: Image.asset("assets/categories/sumo.png"),
                ),
              ),
            ),
          ),
          Container(
            height:100,
            width:500,
            transform: Matrix4.translationValues(0, 380, 0),
            child:Padding(
              padding: const EdgeInsets.symmetric(horizontal: 60.0),
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                strokeWidth: 5.0,
              ),
            ),
          ),
          Container(
              height:200,
              width: 800,
              transform: Matrix4.translationValues(0, 530, 0),
              child: Opacity(
                opacity: AmountOpa,
                child:Text(
                  '$showText',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    decoration : TextDecoration.none,
                    fontWeight: FontWeight.bold,
                    color : Colors.white70,
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }

}

var string= StartText();
StartText()
{
  int min=1;
  int max=8;
  int next = min +Random(new DateTime.now().millisecondsSinceEpoch).nextInt(max - min);
  switch(next)
  {
    case 1:
      return "Życzymy szczęścia ;)";
      break;

    case 2:
      return "Twoj trud się opłaci ;O";
      break;

    case 3:
      return "coz za kompetycje";
      break;

    case 4:
      return "szybkie to logo, nie?";
      break;

    case 5:
      return "tu sie pocałuj";
      break;

    case 6:
      return "made by kompetentni";
      break;

    case 7:
      return "kto dzis wygra? ;)";
      break;

    case 8:
      return "nie ponosimy odpowiedzialnosci za straty w godności ;D";
      break;
  }

}
