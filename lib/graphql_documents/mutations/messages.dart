const REPORT_STRING = r'''
  mutation SendMessage(
    $mRecipient: String!
    $mSubject: String!
    $mText: String!
  ) {
    sendMessage(
      recipientName: $mRecipient
      subject: $mSubject
      text: $mText
    )
    {
        message
        {
          id
        }
    }
  }
''';
