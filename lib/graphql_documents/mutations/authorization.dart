const SIGN_IN_STRING = r'''
    mutation SignIn(
      $username: String!,
      $password: String!,
    ) {
      signIn(
        username: $username,
        password: $password,
        rememberMe: true,
        generateToken: true
      )
      {
        success
        token
      }
    }
     
    ''';
