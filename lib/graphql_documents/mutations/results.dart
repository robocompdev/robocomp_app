const String ADD_MICROMOUSE_SCORE = r'''
  mutation AddMicromouseScore (
    $mPenalty: Int
    $mResult: Int!
    $mRobotId: String!
  ) {
    addMicromouseScore(
      penalty: $mPenalty
      result: $mResult
      robotId: $mRobotId
    )
    {
        micromouseScore
        {
          id
        }
    }
  }
''';

const String ADD_LINEFOLLOWER_SCORE = r'''
  mutation AddLinefollowerScore (
    $mIsFinal: Boolean!
    $mCompetitionId: String!
    $mRobotId: String!
    $mResult: Int!
  ) {
    addLinefollowerScore(
      isFinal: $mIsFinal
      competitionId: $mCompetitionId
      robotId: $mRobotId
      score: $mResult
    )
    {
        linefollowerScore
        {
          id
        }
    }
  }
''';

const String ADD_BEARRESCUE_SCORE = r'''
  mutation AddBearRescueScore (
    $mRobotId: String!
    $mCompetitionId: String!
    $mResult: Int!
    $mCompleted: Boolean!
  ) {
    addBearRescueScore(
      competitionId: $mCompetitionId
      completed: $mCompleted
      robotId: $mRobotId
      result: $mResult
    )
    {
        bearRescueScore
        {
          id
        }
    }
  }
''';

const String DISQUALIFY_ROBOT = r'''
  mutation DisqualifyRobot (
    $mCompetitionId: String!
    $mRobotId: String!
  ) {
    disqualifyRobot(
      competitionId: $mCompetitionId
      robotId: $mRobotId
    ) 
    {
      success
    }
  }
''';