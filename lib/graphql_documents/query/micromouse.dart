const String MICROMOUSE_SCORELIST = r'''
  query MicromouseScoresRanking {
    micromouseScoresRanking {
      edges{
        node{
          id,
          score,
          penalty,
          robot {
            id,
            name,
            team {
              name
            }
            micromouseScores {
              edges{
                node{
                  id
                  score,
                  penalty,
                }
              }
            }
          }
        }
      }
    }
  }
''';
