const String LINEFOLLOWER_QUALIFICATION_PHASE_RESULTS = r'''
    query LinefollowerScoreQualificationRanking (
      $linefollowerType: String!
    ) {
      linefollowerScoreQualificationRanking(linefollowerType: $linefollowerType) {
        edges {
          node {
            score
            robot {
              name
              team {
                name
              }
            }
          }
        }
      }
    }
''';

const String LINEFOLLOWER_FINAL_PHASE_RESULTS = r'''
    query LinefollowerScoreFinalRanking (
      $linefollowerType: String!
    ) {
      linefollowerScoreFinalRanking(linefollowerType: $linefollowerType) {
        edges {
          node {
            score
            robot {
              name
              team {
                name
              }
            }
          }
        }
      }
    }
''';