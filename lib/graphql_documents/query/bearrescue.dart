const String BEARRESCUE_SCORES_RANKING = r'''
    query BearRescueScoresRanking {
      bearRescueScoresRanking {
        edges {
          node {
            score
            robot {
              name
              team {
                name
              }
            }
          }
        }
      }
    }
''';