const String FREESTYLE_FINAL_RANKING = r'''
  query FreestyleScoresFinalRanking{
    freestyleScoresFinalRanking{
      edges{
        node{
          robot{
            name
            team{
              name
            }
          }
          score
        }
      }
    }
  }
''';

const String FREESTYLE_QUALIFICATION_RANKING = r'''
  query {
    freestyleScoresQualificationRanking{
      edges{
        node{
          robot{
            name
            team{
              name
            }
          }
          score
        }
      }
    }
  }
''';