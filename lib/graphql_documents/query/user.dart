const WHO_AM_I_PRIVILEGES_STRING = r'''
    query WhoAmIPrivileges {
      whoAmI
      {
        privilegesLevel
      }
    }     
''';

const WHO_AM_I_USER_INFO = r'''
    query WhoAmIUserInfo {
      whoAmI
      {
        nickname,
        team
        {
          name
        }
      }
    }
''';