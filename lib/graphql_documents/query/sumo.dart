const String SUMO_QUALIFICATION_NOT_ENDED = r'''
  query SumoGroupMatches(
    $group: Int!,
    $type: String!,
    $notEnded: Boolean!
  ){
    sumoGroupMatches(
      group: $group,
      sumoType: $type,
      onlyNotEnded: $notEnded,
    ){
      edges{
        node{
          id,
          robot1{
            name,
            team {
              name
            }
          },
          robot2{
            name,
            team{
              name
            },
          }
        }
      }
    }
  }
''';

const String SUMO_GROUP_COUNT = r'''
  query SumoGroupCount(
    $competitionId: String!
  ){
    sumoGroupCount(competitionId:$$competitionId)
  }
''';