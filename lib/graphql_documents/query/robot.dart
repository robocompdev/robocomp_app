const String ALL_ROBOTS = r'''
    query AllRobots {
      allRobots {
        edges {
          node {
            name
            id
            team{
              name
            }
          }
        }
      }
    }     
''';

const String SEARCH_ROBOT = r'''
    query SearchRobot (
      $qName: String!
      $qId: String
    ) {
      searchRobot ( 
        name: $qName
        competitionId: $qId
      ) {
        edges {
          node {
            name
            id
            team {
              name
            }
          }
        }
      }
    }
''';