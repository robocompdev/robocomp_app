const String LINEFOLLOWERS = r'''
query LinefollowerCompetitions {
  linefollowerCompetitions {
    edges {
      node {
        name
        id
      }
    }
  }
}
''';

const String SUMOS = r'''
query SumoCompetitions {
  sumoCompetitions {
    edges {
      node {
        name
        id
      }
    }
  }
}
''';

const String MICROMOUSES = r'''
query MicromouseCompetition {
  micromouseCompetition {
    edges {
      node {
        name
        id
      }
    }
  }
}
''';

const String BEARRESCUES = r'''
query BearRescueCompetition {
  bearRescueCompetition {
    edges {
      node {
        name
        id
      }
    }
  }
}
''';