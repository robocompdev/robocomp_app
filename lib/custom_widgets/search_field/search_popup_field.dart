import 'package:flutter/material.dart';

typedef ResultsListBuilder = Widget Function(BuildContext, String);

class SearchPopupField<T> extends StatefulWidget {
  SearchPopupField({
    Key key,
    this.listContainerHeight,
    @required this.builder,
    this.backgroundColor,
    this.shape,
    this.searchFieldDecoration,
    this.keyboardType
  });
  final double listContainerHeight;
  final ResultsListBuilder builder;
  final Color backgroundColor;
  final ShapeBorder shape;
  final InputDecoration searchFieldDecoration;
  final TextInputType keyboardType;
  @override
  _SearchPopupFieldState<T> createState() => _SearchPopupFieldState<T>();
}

class _SearchPopupFieldState<T> extends State<SearchPopupField<T>> {
  Widget _popupResultList;
  final _textController = TextEditingController();
  OverlayEntry overlayEntry;
  final LayerLink _layerLink = LayerLink();
  FocusNode _textFieldFocus = FocusNode();
  double listContainerHeight;

  void showMenuList() {
    final RenderBox textFieldRenderBox = context.findRenderObject();
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    final width = textFieldRenderBox.size.width;
    final fieldHeight = textFieldRenderBox.size.height;
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        textFieldRenderBox.localToGlobal(
          textFieldRenderBox.size.topLeft(Offset.zero),
          ancestor: overlay,
        ),
        textFieldRenderBox.localToGlobal(
          textFieldRenderBox.size.topRight(Offset.zero),
          ancestor: overlay,
        ),
      ),
      Offset.zero & overlay.size
    );

    overlayEntry = OverlayEntry(
      builder: (context) {
        return Positioned(
          left: position.left,
          width: width,
          child: CompositedTransformFollower(
            offset: Offset(
              0,
              fieldHeight,
            ),
            showWhenUnlinked: false,
            link: this._layerLink,
            child: Container(
              padding: EdgeInsets.zero,
              alignment: Alignment.topLeft,
              height: listContainerHeight,
              child: Card(
                color: widget.backgroundColor ?? Colors.white,
                shape: widget.shape ?? const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
                child: _popupResultList
              ),
            ),
          ),
        );
      }
    );
    Overlay.of(context).insert(overlayEntry);
  }

  void _searchStringListener() {
    final text = _textController.text;
    if(text.trim().length > 0 && _textFieldFocus.hasFocus) {
      _popupResultList = widget.builder(context, text);
      if(overlayEntry == null) {
        showMenuList();
      } else {
        overlayEntry.markNeedsBuild();
      }
    } else {
      if(overlayEntry != null) overlayEntry.remove();
      overlayEntry = null;
    }
  }

  void _onFocusChange() {
    if(!_textFieldFocus.hasFocus) {
      if(overlayEntry != null) overlayEntry.remove();
      overlayEntry = null;
    }
  }

  @override
  void initState() {
    super.initState();
    _popupResultList = Container();
    _textController.addListener(_searchStringListener);
    _textFieldFocus.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    if(overlayEntry != null) overlayEntry.remove();
    overlayEntry = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    listContainerHeight = widget.listContainerHeight ?? MediaQuery.of(context)
        .size.height/4;
    return CompositedTransformTarget(
      link: _layerLink,
      child: TextField(
        controller: _textController,
        focusNode: _textFieldFocus,
        decoration: widget.searchFieldDecoration ?? const InputDecoration(),
        keyboardType: widget.keyboardType,
      ),
    );
  }
}
