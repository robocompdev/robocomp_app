import 'package:flutter/foundation.dart';

class ElapsedMinutesSeconds {
  final int minutes;
  final int seconds;

  ElapsedMinutesSeconds({
    @required this.minutes,
    @required this.seconds,
  });

  String toString() {
    return minutes.toString().padLeft(2, '0') + ':' +
        seconds.toString().padLeft(2, '0');
  }
}

class ElapsedTime with Comparable<ElapsedTime>{
  int _minutes;
  int _seconds;
  int _centiseconds;

  ElapsedTime({
    @required int minutes,
    @required int seconds,
    @required int centiseconds,
  }):
        _minutes = minutes,
        _seconds = seconds,
        _centiseconds = centiseconds;

  ElapsedTime.fromCentiseconds(int centiseconds) {
    _minutes = centiseconds~/6000;
    _seconds = (centiseconds%6000)~/100;
    _centiseconds = centiseconds%100;

  }
  void addSeconds(int sec) {
    _seconds += sec;
    _minutes += _seconds ~/ 60;
    _seconds = _seconds % 60;
  }

  int get minutes => _minutes;
  int get seconds => _seconds;
  int get centiseconds => _centiseconds;

  @override
  String toString() {
    return _minutes.toString().padLeft(2, '0') + ':' +
        _seconds.toString().padLeft(2, '0') + ':' +
        _centiseconds.toString().padLeft(2, '0');
  }

  int toCentiseconds() {
    return minutes * 6000 + seconds * 100 + centiseconds;
  }

  int compareTo(ElapsedTime other) {
    return toCentiseconds() - other.toCentiseconds();
  }

  bool operator<(ElapsedTime value) =>
      value.minutes*6000 + value.seconds*100 + value.centiseconds <
          minutes*6000 + seconds*100 + centiseconds ? false : true;
}