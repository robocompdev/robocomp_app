import 'package:flutter/material.dart';

class ShimmerSkeletalText extends StatefulWidget {
  final double width;
  final double height;
  final Duration duration;
  final Color backgroundColor;
  final Color gradientColor;

  ShimmerSkeletalText({
    Key key,
    this.width,
    this.height,
    this.duration=const Duration(milliseconds: 1500),
    this.backgroundColor=Colors.white24,
    this.gradientColor=Colors.white30,
  }) : super(key: key);

  @override
  _ShimmerSkeletalTextState createState() => _ShimmerSkeletalTextState();
}

class _ShimmerSkeletalTextState extends State<ShimmerSkeletalText>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  Animation _gradientPos;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    _gradientPos = Tween<double>(
      begin: -20,
      end: 20,
    ).animate(CurvedAnimation(
      curve: Curves.linear,
      parent: _controller,
    ));
    super.initState();
    _controller.repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget child) {
        return Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(_gradientPos.value, 0),
              end: Alignment(-1, 0),
              colors: [
                widget.backgroundColor,
                widget.gradientColor,
                widget.backgroundColor,
              ]
            ),
          ),
        );
      },
    );
  }
}
