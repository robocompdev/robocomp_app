import 'package:flutter/material.dart';

class ButtonStaggerAnimation extends StatelessWidget {
  ButtonStaggerAnimation({
    Key key,
    this.controller,
    this.child,
    this.color,
    this.minWidth,
  }) :
        width = Tween(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0, 1.0,
              curve: Curves.ease
            ),
          ),
        );

  final Widget child;
  final Animation<double> controller;
  final Animation<double> width;
  final Color color;
  final double minWidth;

  Widget _buildAnimation(BuildContext context, Widget child) {
    return LayoutBuilder(
      builder: (BuildContext context,BoxConstraints constraints) {
        final maxWidth = constraints.maxWidth;
        Widget childToShow = child;
        if(controller.status == AnimationStatus.forward) {
          if (width.value > 0.5) {
            childToShow = Container();
          }
        } else if(controller.status == AnimationStatus.reverse) {
          if(width.value > 0.5) {
            childToShow = Container();
          }
        }
          return Align(
            alignment: Alignment.center,
            child: Container(
              width: maxWidth-(maxWidth-minWidth)*width.value,
              child: childToShow,
              alignment: Alignment.center,
              height: 40.0,
              decoration: ShapeDecoration(
                  color: color,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  )
              ),
            ),
          );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      child: child,
      animation: controller,
    );
  }
}
