import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:robocomp_app/custom_widgets/progress_button/button_stagger_animation.dart';


enum PROGRESS_STATE
{
  IDLE, SUCCESS, FAILURE, WORKING,
}


class ProgressButton extends StatefulWidget {

  ProgressButton({
    Key key,
    @required this.duration,
    @required this.idleChild,
    @required this.activeChild,
    @required this.onClick,
    @required this.failureChild,
    @required this.successChild,
    @required this.onSuccess,
    @required this.onFailure,
    @required this.minWidth,
    this.color,
    this.actionDelay = const Duration(milliseconds: 300),
    this.progressButtonController,
  }) :
        super(key: key);

  final Duration duration;
  final Widget idleChild;
  final Widget activeChild;
  final Widget failureChild;
  final Widget successChild;
  final AsyncValueGetter<bool> onClick;
  final VoidCallback onSuccess;
  final VoidCallback onFailure;
  final Duration actionDelay;
  final double minWidth;
  final Color color;
  final ProgressButtonController progressButtonController;

  @override
  _ProgressButtonState createState() => _ProgressButtonState();
}

class _ProgressButtonState extends State<ProgressButton>
    with SingleTickerProviderStateMixin {
  ProgressButtonController _progressButtonController;


  Future<void> _onClick() async {
    try {
      if(_progressButtonController.state != PROGRESS_STATE.IDLE) {
        return;
      }
      await _progressButtonController.controller.forward().orCancel;
      setState(() {
        _progressButtonController.state = PROGRESS_STATE.WORKING;
      });
      final status = await widget.onClick();
      setState(() {
        if(status) {
          _progressButtonController.state = PROGRESS_STATE.SUCCESS;
        } else {
          _progressButtonController.state = PROGRESS_STATE.FAILURE;
        }
      });
      await Future.delayed(widget.actionDelay, () {
        if(status) {
          widget.onSuccess();
        }
      });
      if(!status) {
        widget.onFailure();
      }
      setState(() {
        _progressButtonController.state = PROGRESS_STATE.IDLE;
      });
      await _progressButtonController.controller.reverse().orCancel;
    } on TickerCanceled
    {
      //probably we were disposed
    }
  }

  @override
  void initState() {
    _progressButtonController = widget.progressButtonController ?? ProgressButtonController();
    _progressButtonController.controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    _progressButtonController._onClick = () async {
      _onClick();
    };
    _progressButtonController.state  = PROGRESS_STATE.IDLE;
    super.initState();
  }

  @override
  void dispose() {
    _progressButtonController.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget toShow;
    switch(_progressButtonController.state) {
      case PROGRESS_STATE.IDLE: {
        toShow = widget.idleChild;
        break;
      }
      case PROGRESS_STATE.SUCCESS: {
        toShow = widget.successChild;
        break;
      }
      case PROGRESS_STATE.FAILURE: {
        toShow = widget.failureChild;
        break;
      }
      case PROGRESS_STATE.WORKING: {
        toShow = widget.activeChild;
        break;
      }
    }
    return GestureDetector(
      onTap: _onClick,
      behavior: HitTestBehavior.opaque,
      child: ButtonStaggerAnimation(
        controller: _progressButtonController.controller,
        child: toShow,
        minWidth: widget.minWidth,
        color: widget.color,
      ),
    );
  }
}

class ProgressButtonController {
  AnimationController controller;
  PROGRESS_STATE state;
  AsyncCallback _onClick;

  Future<void> run() => _onClick();

}