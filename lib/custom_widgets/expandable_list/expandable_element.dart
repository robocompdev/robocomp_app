import 'package:flutter/material.dart';
import 'expandable_element_container.dart';


class ExpandableElement extends StatelessWidget {

  final Widget collapsed;
  final Widget expanded;

  final Duration duration;
  final Curve fadeCurve;
  final Curve sizeCurve;
  final double crossFadePoint;
  final AlignmentGeometry alignment;

  ExpandableElement({
    Key key,
    @required
    this.collapsed,
    @required
    this.expanded,
    this.crossFadePoint = 0.5,
    this.fadeCurve = Curves.linear,
    this.sizeCurve = Curves.fastOutSlowIn,
    this.alignment = Alignment.topLeft,
    this.duration = const Duration(milliseconds: 400)
  }) :
      super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = ExpandableElementContainerController.of(context);
    final double collapsedFadeStart = crossFadePoint < 0.5 ? 0 : (crossFadePoint * 2 - 1);
    final double collapsedFadeEnd = crossFadePoint < 0.5 ? 2 * crossFadePoint : 1;
    final double expandedFadeStart = crossFadePoint < 0.5 ? 0 : (crossFadePoint * 2 - 1);
    final double expandedFadeEnd = crossFadePoint < 0.5 ? 2 * crossFadePoint : 1;

    return AnimatedCrossFade(
      alignment: this.alignment,
      firstChild: collapsed,
      secondChild: expanded,
      firstCurve: Interval(collapsedFadeStart, collapsedFadeEnd, curve: fadeCurve),
      secondCurve: Interval(expandedFadeStart, expandedFadeEnd, curve: fadeCurve),
      sizeCurve: sizeCurve,
      crossFadeState: controller.expanded() == key ? CrossFadeState.showSecond : CrossFadeState.showFirst,
      duration: duration,
    );
  }
}
