import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExpandableElementContainer extends StatelessWidget {

  final ExpandableElementContainerController controller;
  final Widget child;

  ExpandableElementContainer({
    Key key,
    ExpandableElementContainerController controller,
    @required
    this.child
  }) :
        this.controller = controller ?? ExpandableElementContainerController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return _ExpandableElementContainerInheritedNotifier(
      controller: controller,
      child: child,
    );
  }
}

class ExpandableElementContainerController extends ValueNotifier<Key> {

  Key expanded() {
    return value;
  }

  void expand(Key key) {
    value = key;
  }

  void collapse(Key key) {
    if(value == key) {
      key = null;
    }
  }

  void toggle(Key key) {
    if(value == key) {
        value = null;
      }
    else {
      value = key;
    }

  }

  ExpandableElementContainerController({
    Key initialExpanded
  }) :
      super(initialExpanded);

  static ExpandableElementContainerController of(BuildContext context) {
    final controller = context.inheritFromWidgetOfExactType(_ExpandableElementContainerInheritedNotifier);
    return (controller as _ExpandableElementContainerInheritedNotifier)?.notifier;
  }
}

class _ExpandableElementContainerInheritedNotifier
    extends InheritedNotifier<ExpandableElementContainerController> {

  _ExpandableElementContainerInheritedNotifier({
    @required
    ExpandableElementContainerController controller,
    @required
    Widget child
  }) :
      super(notifier: controller, child: child);
}