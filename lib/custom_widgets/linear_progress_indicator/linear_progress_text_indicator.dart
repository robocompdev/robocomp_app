import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LinearProgressTextIndicator extends StatefulWidget {
  final Color backgroundColor;
  final Color progressColor;
  final double progress;
  final double lineWidth;
  final Widget child;

  @override
  _LinearProgressTextIndicatorState createState() =>
      _LinearProgressTextIndicatorState();

  LinearProgressTextIndicator({
      this.backgroundColor = Colors.deepOrange,
      this.progressColor = Colors.red,
      @required this.progress,
      @required this.lineWidth,
      @required this.child,
  });
}

class _LinearProgressTextIndicatorState extends State<LinearProgressTextIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40.0),
        child: widget.child,
      ),
      painter: LinearProgressPainter(
          backgroundColor: widget.backgroundColor,
          progressColor: widget.progressColor,
          progress: widget.progress,
          lineWidth: widget.lineWidth),
    );
  }
}

class LinearProgressPainter extends CustomPainter {
  final Color backgroundColor;
  final Color progressColor;
  final double progress;
  final double lineWidth;

  final Paint _backgroundLinePaint = Paint();
  final Paint _progressLinePaint = Paint();


  LinearProgressPainter({
    @required this.backgroundColor,
    @required this.progressColor,
    @required this.progress,
    @required this.lineWidth
  }) {
    _backgroundLinePaint.color = backgroundColor;
    _backgroundLinePaint.strokeCap = StrokeCap.butt;
    _backgroundLinePaint.style = PaintingStyle.stroke;
    _backgroundLinePaint.strokeWidth = lineWidth;

    _progressLinePaint.style = PaintingStyle.stroke;
    _progressLinePaint.strokeWidth = lineWidth;
    _progressLinePaint.strokeCap = StrokeCap.butt;

    _progressLinePaint.color = progressColor;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final start = Offset(0.0, size.height / 2);
    final end = Offset(size.width, size.height / 2);
    canvas.drawLine(start, end, _backgroundLinePaint);

    final widthProgress = size.width * progress;
    canvas.drawLine(
        start, Offset(widthProgress, size.height / 2), _progressLinePaint);
  }

  @override
  bool shouldRepaint(LinearProgressPainter oldDelegate) {
    return backgroundColor != oldDelegate.backgroundColor
        || progressColor != oldDelegate.progressColor
        || progress != oldDelegate.progress
        || lineWidth != oldDelegate.lineWidth;
  }

  @override
  bool shouldRebuildSemantics(LinearProgressPainter oldDelegate) {
    return true;
  }
}