import 'package:flutter/widgets.dart';
import 'package:robocomp_app/graphql_documents/query/competition.dart';


enum CompetitionCategory {
  MicroMouse,
//  LegoSumo,
//  StandardSumo,
//  MiniSumo,
//  MicroSumo,
//  NanoSumo,
  LinefollowerEnhanced,
  Linefollower,
  LinefollowerLight,
  LegoLF,
  BearRescue,
  Freestyle,
}

const Map<String, CompetitionCategory> competitionEnums = {
  "Micro Mouse": CompetitionCategory.MicroMouse,
//  "Lego Sumo": CompetitionCategory.LegoSumo,
//  "Standard Sumo": CompetitionCategory.StandardSumo,
//  "Mini Sumo": CompetitionCategory.MiniSumo,
//  "Micro Sumo": CompetitionCategory.MicroSumo,
//  "Nano Sumo": CompetitionCategory.NanoSumo,
  "Linefollower Enhanced": CompetitionCategory.LinefollowerEnhanced,
  "Linefollower": CompetitionCategory.Linefollower,
  "Linefollower Light": CompetitionCategory.LinefollowerLight,
  "Lego LineFollower": CompetitionCategory.LegoLF,
  "Bear Rescue": CompetitionCategory.BearRescue,
  "Freestyle": CompetitionCategory.Freestyle,
};

const Map<CompetitionCategory, String> competitionNames = {
  CompetitionCategory.MicroMouse: "Micro Mouse",
//  CompetitionCategory.LegoSumo: "Lego Sumo",
//  CompetitionCategory.StandardSumo: "Standard Sumo",
//  CompetitionCategory.MiniSumo: "Mini Sumo",
//  CompetitionCategory.MicroSumo: "Micro Sumo",
//  CompetitionCategory.NanoSumo: "Nano Sumo",
  CompetitionCategory.LinefollowerEnhanced: "Linefollower Enhanced",
  CompetitionCategory.Linefollower: "Linefollower",
  CompetitionCategory.LinefollowerLight: "Linefollower Light",
  CompetitionCategory.LegoLF: "Lego LineFollower",
  CompetitionCategory.BearRescue: "Bear Rescue",
  CompetitionCategory.Freestyle: "Freestyle",
};


const Map<CompetitionCategory, AssetImage> competitionImage = {
  CompetitionCategory.MicroMouse: AssetImage("assets/categories/micromouse.png"),
  //CompetitionCategory.LegoSumo: AssetImage("assets/categories/sumo.png"),
  //CompetitionCategory.StandardSumo: AssetImage("assets/categories/sumo.png"),
  //CompetitionCategory.MiniSumo: AssetImage("assets/categories/sumo.png"),
  //CompetitionCategory.MicroSumo: AssetImage("assets/categories/sumo.png"),
  //CompetitionCategory.NanoSumo: AssetImage("assets/categories/sumo.png"),
  CompetitionCategory.LinefollowerEnhanced: AssetImage("assets/categories/linefollower.png"),
  CompetitionCategory.Linefollower: AssetImage("assets/categories/linefollower.png"),
  CompetitionCategory.LinefollowerLight: AssetImage("assets/categories/linefollower.png"),
  CompetitionCategory.LegoLF: AssetImage("assets/categories/linefollower.png"),
  CompetitionCategory.BearRescue: AssetImage("assets/categories/bear.png"),
  CompetitionCategory.Freestyle: AssetImage("assets/categories/freestyle.png"),
};

enum CompetitionType {
  MICROMOUSE,
  LINEFOLLOWER,
  BEARRESCUE,
  FREESTYLE,
  //SUMO,
  NOT_IMPLEMENTED,
}

final Map<String, String> competitionRoutes = {
  "Micro Mouse":     '/micromouse_arbiter_view',
  "Linefollower":   'linefollower',
  //"Sumo":           '/sumo_arbiter_view',
  "Bear Rescue":     'bearrescue',
};

final Map<String, String> competitionQueries = {
  "Micro Mouse": MICROMOUSES,
  "Linefollower": LINEFOLLOWERS,
  //"Sumo": SUMOS,
  "Bear Rescue": BEARRESCUES,
};

final Map<String, String> competitionQueryName = {
  "Micro Mouse":     "micromouseCompetition",
  "Linefollower":   "linefollowerCompetitions",
  //"Sumo":           "sumoCompetitions",
  "Bear Rescue":     "bearRescueCompetition",
};

final Map<CompetitionCategory, String> backendCompetitionNames = {
  //CompetitionCategory.MicroSumo: 'micro_sumo',
  //CompetitionCategory.LegoSumo: 'lego_sumo',
  //CompetitionCategory.StandardSumo: 'standard_sumo',
  //CompetitionCategory.MiniSumo: 'mini_sumo',
  //CompetitionCategory.NanoSumo: 'nano_sumo',
  CompetitionCategory.LegoLF: 'lego_linefollower',
  CompetitionCategory.LinefollowerEnhanced: 'enhanced_linefollower',
  CompetitionCategory.LinefollowerLight: 'light_linefollower',
  CompetitionCategory.Linefollower: 'linefollower',

};

final Map<CompetitionCategory, CompetitionType> competitionTypeMap = {
  CompetitionCategory.MicroMouse: CompetitionType.MICROMOUSE,
  //CompetitionCategory.LegoSumo: CompetitionType.SUMO,
  //CompetitionCategory.StandardSumo: CompetitionType.SUMO,
  //CompetitionCategory.MiniSumo: CompetitionType.SUMO,
  //CompetitionCategory.MicroSumo: CompetitionType.SUMO,
  //CompetitionCategory.NanoSumo: CompetitionType.SUMO,
  CompetitionCategory.LinefollowerEnhanced: CompetitionType.LINEFOLLOWER,
  CompetitionCategory.Linefollower: CompetitionType.LINEFOLLOWER,
  CompetitionCategory.LinefollowerLight: CompetitionType.LINEFOLLOWER,
  CompetitionCategory.LegoLF: CompetitionType.LINEFOLLOWER,
  CompetitionCategory.BearRescue: CompetitionType.BEARRESCUE,
  CompetitionCategory.Freestyle: CompetitionType.FREESTYLE,
};
