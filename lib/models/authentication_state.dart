import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:robocomp_app/config.dart';
import 'package:robocomp_app/graphql_documents/mutations/authorization.dart';
import 'package:robocomp_app/graphql_documents/query/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum PRIVILEGES_LEVEL {
  NOT_AUTHORIZED, GUEST, PARTICIPANT, ARBITER, ADMIN
}

const Map<String, PRIVILEGES_LEVEL> privilegesRepositoryMap = {
  'PARTICIPANT': PRIVILEGES_LEVEL.PARTICIPANT,
  'ARBITER': PRIVILEGES_LEVEL.ARBITER,
  'ADMIN': PRIVILEGES_LEVEL.ADMIN,
};

const String TOKEN_LOCAL_KEY = 'AuthToken';

class AuthenticationState extends ChangeNotifier{
  String authorizationToken;
  PRIVILEGES_LEVEL privilegesLevel;

  AuthenticationState() :
      this.authorizationToken = '',
      this.privilegesLevel = PRIVILEGES_LEVEL.NOT_AUTHORIZED;

  Future<bool> restoreLocalAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(TOKEN_LOCAL_KEY)) {
      final authToken = prefs.getString(TOKEN_LOCAL_KEY);

      final HttpLink httpLink = HttpLink(
        uri: GRAPHQL_URL,
      );
      final AuthLink authLink = AuthLink(
        getToken: () async => 'Bearer $authToken',
      );
      final Link link = authLink.concat(httpLink);

      ValueNotifier<GraphQLClient> client = ValueNotifier(
        GraphQLClient(
          link: link,
          cache: InMemoryCache(),
        ),
      );

      final QueryOptions privilegesQuery = QueryOptions(
        document: WHO_AM_I_PRIVILEGES_STRING,
      );

      final detailsResult = await client.value.query(privilegesQuery);
      if (detailsResult.hasException) {
        print(detailsResult.exception.toString());
        return false;
        //todo: this should not happen, network error
      }
      privilegesLevel = privilegesRepositoryMap[
      detailsResult.data['whoAmI']['privilegesLevel']
      ];
      authorizationToken = authToken;

      notifyListeners();
      return true;
    }
    return false;
  }

  Future<bool> signIn(
      final String username,
      final String password,
  ) async {
    final HttpLink httpLink = HttpLink(
      uri: GRAPHQL_URL,
    );

    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        link: httpLink,
        cache: InMemoryCache(),
      ),
    );
    final options = MutationOptions(
      document: SIGN_IN_STRING,
      variables: <String, dynamic>{
        'username': username,
        'password': password,
      },
    );

    final authResult = await client.value.mutate(options);
    if (authResult.hasException) {
      return false;
      //todo: check error type
    }
    if(authResult.data['signIn']['success'] ==  false)
      return false;

    authorizationToken=authResult.data['signIn']['token'] as String;

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer $authorizationToken',
    );
    final Link link = authLink.concat(httpLink);

    client.value = GraphQLClient(
      link: link,
      cache: InMemoryCache(),
    );

    final QueryOptions privilegesQuery = QueryOptions(
      document: WHO_AM_I_PRIVILEGES_STRING,
    );

    final detailsResult = await client.value.query(privilegesQuery);
    if (detailsResult.hasException) {
      print(detailsResult.exception.toString());
      return false;
      //todo: this should not happen, network error
    }
    privilegesLevel = privilegesRepositoryMap[
      detailsResult.data['whoAmI']['privilegesLevel']
    ];

    //local auth token storage
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(TOKEN_LOCAL_KEY, authorizationToken);
    notifyListeners();
    return true;
  }

  void signInAsGuest() {
    authorizationToken = '';
    privilegesLevel = PRIVILEGES_LEVEL.GUEST;
    notifyListeners();
  }

  bool isAuthenticated() {
    return privilegesLevel != PRIVILEGES_LEVEL.NOT_AUTHORIZED
        && privilegesLevel != PRIVILEGES_LEVEL.GUEST;
  }

  Future<void> signOut() async {
    authorizationToken='';
    privilegesLevel=PRIVILEGES_LEVEL.NOT_AUTHORIZED;

    //removing token from local storage
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(TOKEN_LOCAL_KEY);

    notifyListeners();
  }

}