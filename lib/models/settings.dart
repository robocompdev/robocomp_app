import 'package:flutter/widgets.dart';
import 'package:robocomp_app/models/authentication_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String ARBITER_MODE_LOCAL_KEY = 'arbiterMode';

class Settings extends ChangeNotifier {
  bool arbiterMode;

  Settings() :
      arbiterMode = false;

  Future<bool> activateArbiterMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    arbiterMode = true;
    prefs.setBool(ARBITER_MODE_LOCAL_KEY, true);
    notifyListeners();
    return true;
  }

  Future<bool> deactivateArbiterMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    arbiterMode = false;
    prefs.setBool(ARBITER_MODE_LOCAL_KEY, false);
    notifyListeners();
    return true;
  }

  Future<bool> restoreArbiterModeState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.containsKey(ARBITER_MODE_LOCAL_KEY)) {
      arbiterMode = prefs.getBool(ARBITER_MODE_LOCAL_KEY);
      notifyListeners();
      return true;
    }
    return false;
  }
}