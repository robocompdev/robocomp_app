import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:robocomp_app/config.dart';
import 'package:robocomp_app/models/authentication_state.dart';
import 'package:robocomp_app/models/settings.dart';
import 'package:robocomp_app/pages/arbiter_view/arbiter_page.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/beerrescue.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/race_arbiter_page.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/micromouse/micromouse_stopwatch.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/sumo/sumo_match_page.dart';
import 'package:robocomp_app/pages/arbiter_view/robot_scanner/robot_scanner.dart';
import 'package:robocomp_app/pages/categories/bearrescue/bear_rescue_view.dart';
import 'package:robocomp_app/pages/categories/freestyle/freestyle_results_page.dart';
import 'package:robocomp_app/pages/categories/linefollower/linefollower_results_page.dart';
import 'package:robocomp_app/pages/categories/micromouse/micromouse_view.dart';
import 'package:robocomp_app/pages/categories/sumo/sumo_results_page.dart';
import 'package:robocomp_app/pages/category/category_page.dart';
import 'package:robocomp_app/pages/login_page.dart';
import 'package:robocomp_app/drawer/robot_details.dart';
import 'package:robocomp_app/pages/settings/settings_page.dart';
import 'package:robocomp_app/pages/splash_screen.dart';
import 'package:robocomp_app/pages/arbiter_view/categories/linefollower.dart';
import 'generated/i18n.dart';

void main() => runApp(RobocompApp());

class RobocompApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final routesMap = <String, WidgetBuilder>{
      '/': (context) => SplashScreen(),
      '/login_page': (context) => LoginPage(),
      '/category_page': (context) => CategoryPage(),
      '/robot_details': (context) => RobotDetails(),
      '/robot_scanner': (context) => RobotScanner(),
      '/settings_page': (context) => SettingsPage(),
      '/arbiter_page': (context) => ArbiterPage(),
        '/race_arbiter_page': (context) => RaceArbiterPage(),
          '/micromouse_stopwatch': (context) => MicromouseStopwatchPage(),
          '/linefollower_stopwatch': (context) => LinefollowerStopwatchPage(),
          '/bearrescue_stopwatch': (context) => BearRescueStopwatchPage(),
      '/sumo_arbiter_view': (context) => SumoMatchPage(),
      '/micromouse_results_view': (context) => MicromouseView(),
      '/sumo_results_page': (context) => SumoResultsPage(),
      '/linefollower_results_page': (context) => LinefollowerResultsPage(),
      '/bear_rescue_results_view': (context) => BearRescueView(),
      '/freestyle_results_page': (context) => FreestyleResultsPage(),
    };
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

    final Link httpLink = HttpLink(
      uri: GRAPHQL_URL,
    );

    return  ChangeNotifierProvider(
      create: (context) => AuthenticationState(),
      child: ChangeNotifierProvider(
        create: (context) => Settings(),
        child: Consumer<AuthenticationState>(
          builder: (context, state, child) {
            Link link = httpLink;
            if(state.isAuthenticated()) {
              final token = state.authorizationToken;
              final AuthLink authLink = AuthLink(
                getToken: () async => 'Bearer $token',
              );
              link = authLink.concat(httpLink);
            }

            ValueNotifier<GraphQLClient> client = ValueNotifier(
              GraphQLClient(
                link: link,
                cache: InMemoryCache(),
              ),
            );

            return GraphQLProvider(
              client: client,
              child: child,
            );
          },
          child: MaterialApp(
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: S.delegate.supportedLocales,
            localeResolutionCallback: S.delegate.resolution(fallback: Locale("en")),
            initialRoute: "/",
            routes: routesMap,
            title: 'Robocomp',
            theme: ThemeData(
              primaryColor: Color(0xff000719),
              primaryTextTheme: TextTheme(
                  title: TextStyle(
                    color: Colors.white70,
                  ),
                  button: TextStyle(color: Colors.white30)),
              appBarTheme: AppBarTheme.of(context).copyWith(
                  color: Color(0xff000719)
              ),
            ),
          ),
        ),
      ),
    );
  }
}
